import Professional from "./professional.model";

//@ts-ignore
export const findAll = async () => await Professional.findAll();

//@ts-ignore
export const findOne = async (body: any) => 
    //@ts-ignore
    await Professional.findOne({
        where: {
            Cedula: body.Cedula,
        },
    });

//@ts-ignore
export const findById = async(id: number) => await Professional.findByPk(id);