import { Router } from "express";
import { requireJwtMiddleware } from "../../middleware/auth.middleware";
import { getProfessionals, getProfessional, postProfessional, putProfessional, deleteProfessional } from "./professional.service";

/**
 * Router Definition
 */

const professionalsRouter = Router();

professionalsRouter.use(requireJwtMiddleware);


professionalsRouter.get("/", getProfessionals);
professionalsRouter.get("/:id", getProfessional);
professionalsRouter.post("/", postProfessional);
professionalsRouter.put("/:id", putProfessional);
professionalsRouter.delete("/:id", deleteProfessional);

export default professionalsRouter;