/**
 * Data Model Interfaces
 */
 import bodyParser from "body-parser";
import { Request, Response } from "express";
 import Professional from "./professional.model";
 import * as query from "./querys";

 /**
 * Service Methods
 */

 export const getProfessionals = async (req: Request, res: Response) => {
     
    //@ts-ignore

     const professionals = await query.findAll();

     res.json({ professionals });
 };

 export const getProfessional = async (req: Request, res: Response) => {
    const { id } = req.params;

    //@ts-ignore

   const professional = await query.findById(id);

   if (professional) {
       res.json(professional);
   } else {
       res.status(200).json({
           msg: `No existe un profesional con la cédula ${id}`
       });
   }
};

export const postProfessional = async (req: Request, res: Response) => {
   const { body } = req;

   try {
       //@ts-ignore
       const existsCedula = await query.findOne(body);

       if (existsCedula) {
           return res.status(400).json({
               msg: "Ya existe un profesional con el código " + body.Cedula,
           });
       }

       //@ts-ignore
       const professional = Professional.build({
            Cedula: body.Cedula,
            Nombre: body.Nombre,
            Apellido: body.Apellido,
            Telefono: body.Telefono,
            Estado: body.Estado,
            Correo: body.Correo,
       });

       await professional.save();

       res.json(professional);

   } catch (error) {
       console.log(error);
       res.status(500).json({
           msg: "Hable con el administrador",
       });
   }
};

export const putProfessional = async (req: Request, res: Response) => {
   const { id } = req.params;
   const { body } = req;

   try {
       //@ts-ignore
       var professional = await query.findById(id);
       if (!professional) {
           //@ts-ignore
           professional = Professional.build({
                Cedula: body.Cedula,
                Nombre: body.Nombre,
                Apellido: body.Apellido,
                Telefono: body.Telefono,
                Estado: body.Estado,
                Correo: body.Correo,
           });

           await professional.save();

       } else {
           
           await professional.update(body);

       }

       res.json(professional);

   } catch (error) {
       console.log(error);
       res.status(500).json({
           msg: "Hable con el administrador",
       });
   }
};

export const deleteProfessional = async (req: Request, res: Response) => {
   const { id } = req.params;

   //@ts-ignoreclear

   const professional = await query.findById(id);

   if (!professional) {
       return res.status(200).json({
           msg: "No existe un profesional con el código " + id,
       });
   }

   await professional.update({ estado: false });

   await professional.destroy();

   res.json(professional);
};