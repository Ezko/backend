//importamos sequelize y la conexion a la base de datos
import { DataTypes, Model } from "sequelize";
import db from "../../db/connection";

class Professional extends Model {
  public Cedula!: string;
  public Nombre!: string;
  public Apellido!: string;
  public Telefono!: string;
  public Estado!: string;
  public Correo!: string;
}

//@ts-ignore
Professional.init(
  {
    Cedula: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    Nombre: {
      type: DataTypes.STRING,
    },
    Apellido: {
      type: DataTypes.STRING,
    },
    Telefono: {
      type: DataTypes.STRING,
    },
    Estado: {
      type: DataTypes.STRING,
    },
    Correo: {
      type: DataTypes.STRING,
    },

  },
  {
    //@ts-ignore
    tableName: "Profesional",
    sequelize: db,
  }
)

//exportamos como default
export default Professional;