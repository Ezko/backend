import { Router } from "express";
import { requireJwtMiddleware } from "../../middleware/auth.middleware";
import { getChronicPatients, getChronicPatient, postChronicPatient, putChronicPatient, deleteChronicPatient } from "./chronicPatient.service";

/**
 * Router Definition
 */

const chronicPatientRouter = Router();

chronicPatientRouter.use(requireJwtMiddleware);


chronicPatientRouter.get("/", getChronicPatients);
chronicPatientRouter.get("/:id", getChronicPatient);
chronicPatientRouter.post("/", postChronicPatient);
chronicPatientRouter.put("/:id", putChronicPatient);
chronicPatientRouter.delete("/:id", deleteChronicPatient);

export default chronicPatientRouter;