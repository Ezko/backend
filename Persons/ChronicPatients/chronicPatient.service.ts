/**
 * Data Model Interfaces
 */
 import { Request, Response } from "express";
import { QueryTypes } from "sequelize/dist";
 import ChronicPatient from "./chronicPatient.model";
 import Patient from "../Patients/patient.model";
 import * as query from "./querys";
 import * as queryPatients from "../Patients/querys";
import * as queryAcutePatients from "../AcutePatients/querys"
import db from "../../db/connection"
import PatientMarker from "../PatientMarker/patientMarker.model";

 /**
 * Service Methods
 */

 export const getChronicPatients = async (req: Request, res: Response) => {
     
    //@ts-ignore

     const patients = await query.getAll();

     res.json({ patients });
 };

 export const getChronicPatient = async (req: Request, res: Response) => {
     const { id } = req.params;

     //@ts-ignore

    const patient = await query.findById(id);

    if (patient) {
        res.json(patient);
    } else {
        res.status(200).json({
            msg: `No existe un paciente con el documento ${id}`
        });
    }
};

export const postChronicPatient = async (req: Request, res: Response) => {
    const { body } = req;
    let t;
    try {
        //@ts-ignore
        const existsID = await query.findOne(body);
        const existsPatient = await queryPatients.findOne(body);
        const existsAcutePatient = await queryAcutePatients.findOne(body);

        if (existsAcutePatient) {
            return res.status(400).json({
                msg: "Un paciente no puede ser crónico y agudo al mismo tiempo.",
            });
        }

        if (existsID || existsPatient) {
            return res.status(400).json({
                msg: "Ya existe un paciente con el documento " + body.Documento,
            });
        }

        //@ts-ignore
        const patient = Patient.build({
            Documento: body.Documento,
            Estado: body.Estado,
            Nombre: body.Nombre,
            Apellido: body.Apellido,
            TelPrincipal: body.TelPrincipal,
            TelSecundario: body.TelSecundario,
            Correo: body.Correo,
        });

        //@ts-ignore
        const chronicPatient = ChronicPatient.build({
            Documento: body.Documento,
            NumeroFondo: body.NumeroFondo,
        });
        
        
        //@ts-ignore
        t = await db.transaction();

        //@ts-ignore
        await patient.save(), { t };

        //@ts-ignore
        await chronicPatient.save(), { t };

        let counter = 0;
        while (counter<body.MarcadoresVirales.length) {
            //@ts-ignores
            await PatientMarker.create({
                Documento: body.Documento,
                MarcadorViral: body.MarcadoresVirales[counter]
            }, { transaction: t })
            counter++;
        }

        //@ts-ignore
        t.commit();

        res.json([patient, chronicPatient]);
        

        

    } catch (error) {
        if(t){await t.rollback()}
        console.log(error);
        res.status(500).json({
            msg: "Hable con el administrador",
        });
    }
};

export const putChronicPatient = async (req: Request, res: Response) => {
    const { id } = req.params;
    const { body } = req;

    try {
        //@ts-ignore
        var patient = await query.findById(id);
        if (!patient) {
            //@ts-ignore
            patient = ChronicPatient.build({
                Documento: body.Documento,
                NumeroFondo: body.NumeroFondo,
            });

            await patient.save();

        } else {
            
            await patient.update(body);

        }

        res.json(patient);

    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Hable con el administrador",
        });
    }
};

export const deleteChronicPatient = async (req: Request, res: Response) => {
    const { id } = req.params;

    //@ts-ignoreclear

    const patient = await query.findById(id);

    if (!patient) {
        return res.status(200).json({
            msg: `No existe un paciente con el documento ${id}`
        });
    }

    await patient.update({ estado: false });

    await patient.destroy();

    res.json(patient);
};