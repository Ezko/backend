//importamos sequelize y la conexion a la base de datos
import { DataTypes, Model } from "sequelize";
import db from "../../db/connection";

class ChronicPatient extends Model {
  public Documento!: string;
  public NumeroFondo!: string;
}

//@ts-ignore
ChronicPatient.init(
  {
    Documento: {
          type: DataTypes.STRING,
          primaryKey: true,
      },
    NumeroFondo: {
      type: DataTypes.STRING,
    }
  },
  {
      //@ts-ignore
      tableName: "Cronico",
      sequelize: db,
  },
)

//exportamos como default
export default ChronicPatient;