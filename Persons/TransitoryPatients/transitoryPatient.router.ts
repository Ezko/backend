import { Router } from "express";
import dialysisRouter from "../../Coordination/Dialysis/dialysis.router";
import { requireJwtMiddleware } from "../../middleware/auth.middleware";
import { getTransitoryPatients, getTransitoryPatient, postTransitoryPatient, putTransitoryPatient, deleteTransitoryPatient } from "./transitoryPatient.service";

/**
 * Router Definition
 */

const transitoryPatientRouter = Router();

dialysisRouter.use(requireJwtMiddleware);


transitoryPatientRouter.get("/", getTransitoryPatients);
transitoryPatientRouter.get("/:id", getTransitoryPatient);
transitoryPatientRouter.post("/", postTransitoryPatient);
transitoryPatientRouter.put("/:id", putTransitoryPatient);
transitoryPatientRouter.delete("/:id", deleteTransitoryPatient);

export default transitoryPatientRouter;