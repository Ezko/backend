/**
 * Data Model Interfaces
 */
 import { Request, Response } from "express";
import { QueryTypes } from "sequelize/dist";
 import TransitoryPatient from "./transitoryPatient.model";
 import Patient from "../Patients/patient.model";
 import * as query from "./querys";
 import * as queryChronicPatients from "../ChronicPatients/querys"
 import * as queryPatients from "../Patients/querys";
import * as queryAcutePatients from "../AcutePatients/querys";
import db from "../../db/connection";
import PatientMarker from "../PatientMarker/patientMarker.model";

 /**
 * Service Methods
 */

 export const getTransitoryPatients = async (req: Request, res: Response) => {
     
    //@ts-ignore

     const patients = await query.getAll();

     res.json({ patients });
 };

 export const getTransitoryPatient = async (req: Request, res: Response) => {
     const { id } = req.params;

     //@ts-ignore

    const patient = await query.findById(id);

    if (patient) {
        res.json(patient);
    } else {
        res.status(200).json({
            msg: `No existe un paciente con el documento ${id}`
        });
    }
};

export const postTransitoryPatient = async (req: Request, res: Response) => {
    const { body } = req;
    let t;
    try {
        //@ts-ignore
        const existsID = await query.findOne(body);
        const existsPatient = await queryPatients.findOne(body);
        const existsChronicPatient = await queryChronicPatients.findOne(body);
        const existsAcutePatient = await queryAcutePatients.findOne(body);

        if (existsChronicPatient) {
            return res.status(400).json({
                msg: "Un paciente no puede ser transitorio y crónico al mismo tiempo.",
            });
        }

        if (existsAcutePatient) {
            return res.status(400).json({
                msg: "Un paciente no puede ser transitorio y agudo al mismo tiempo.",
            });
        }

        if (existsID || existsPatient) {
            return res.status(400).json({
                msg: "Ya existe un paciente con el documento " + body.Documento,
            });
        }

        //@ts-ignore
        const patient = Patient.build({
            Documento: body.Documento,
            Estado: body.Estado,
            Nombre: body.Nombre,
            Apellido: body.Apellido,
            TelPrincipal: body.TelPrincipal,
            TelSecundario: body.TelSecundario,
            Correo: body.Correo,
        });

        //@ts-ignore
        const transitoryPatient = TransitoryPatient.build({
            Documento: body.Documento,
            NumeroSeguro: body.NumeroSeguro,
            FechaIngreso: body.FechaIngreso,
            FechaEgreso: body.FechaEgreso,
        });

        //@ts-ignore
        t = await db.transaction();
        await patient.save(), {t};
        await transitoryPatient.save(), { t };
        
        let counter = 0;
        while (counter<body.MarcadoresVirales.length) {
            //@ts-ignores
            await PatientMarker.create({
                Documento: body.Documento,
                MarcadorViral: body.MarcadoresVirales[counter]
            }, { transaction: t })
            counter++;
        }
        //@ts-ignore
        t.commit();

        res.json([patient, transitoryPatient]);

        

    } catch (error) {
        if(t){await t.rollback()}
        console.log(error);
        res.status(500).json({
            msg: "Hable con el administrador",
        });
    }
};

export const putTransitoryPatient = async (req: Request, res: Response) => {
    const { id } = req.params;
    const { body } = req;

    try {
        //@ts-ignore
        var patient = await query.findById(id);
        if (!patient) {
            //@ts-ignore
            patient = TransitoryPatient.build({
                Documento: body.Documento,
                NumeroSeguro: body.NumeroSeguro,
                FechaIngreso: body.FechaIngreso,
                FechaEgreso: body.FechaEgreso,
            });

            await patient.save();

        } else {
            
            await patient.update(body);

        }

        res.json(patient);

    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Hable con el administrador",
        });
    }
};

export const deleteTransitoryPatient = async (req: Request, res: Response) => {
    const { id } = req.params;

    //@ts-ignoreclear

    const patient = await query.findById(id);

    if (!patient) {
        return res.status(200).json({
            msg: `No existe un paciente con el documento ${id}`
        });
    }

    await patient.update({ estado: false });

    await patient.destroy();

    res.json(patient);
};