//importamos sequelize y la conexion a la base de datos
import { DataTypes, Model } from "sequelize";
import db from "../../db/connection";

class TransitoryPatient extends Model {
    public Documento!: string;
    public NumeroSeguro!: string;
    public FechaIngreso!: Date;
    public FechaEgreso!: Date;
  }
  
//@ts-ignore
TransitoryPatient.init(
    {
        Documento: {
            type: DataTypes.STRING,
            primaryKey: true,
        },
        NumeroSeguro: {
            type: DataTypes.STRING,
        },
        FechaIngreso: {
            type: DataTypes.DATE,
        },
        FechaEgreso: {
            type: DataTypes.DATE,
        },
    },
    {
        //@ts-ignore
        tableName: "Transitorio",
        sequelize: db,
    },
  )

//exportamos como default
export default TransitoryPatient;