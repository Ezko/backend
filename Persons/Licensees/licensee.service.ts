/**
 * Data Model Interfaces
 */
 import { Request, Response } from "express";
import { QueryTypes } from "sequelize/dist";
 import Licensee from "./licensee.model";
 import * as query from "./querys";
 import Professional from "../Professionals/professional.model";

 /**
 * Service Methods
 */

 export const getLicensees = async (req: Request, res: Response) => {
     
    //@ts-ignore

     const licensees = await query.getAll();

     res.json({ licensees });
 };

 export const getLicensee = async (req: Request, res: Response) => {
     const { id } = req.params;

     //@ts-ignore

    const licensee = await query.findById(id);

    if (licensee) {
        res.json(licensee);
    } else {
        res.status(200).json({
            msg: `No existe un licenciado con la cédula ${id}`
        });
    }
};

export const postLicensee = async (req: Request, res: Response) => {
    const { body } = req;

    try {
        //@ts-ignore
        const existsID = await query.findOne(body);

        if (existsID) {
            return res.status(400).json({
                msg: "Ya existe un licenciado con la cédula " + body.Cedula,
            });
        }

        //@ts-ignore
        const professional = Professional.build({
            Cedula: body.Cedula,
            Nombre: body.Nombre,
            Apellido: body.Apellido,
            Telefono: body.Telefono,
            Estado: body.Estado,
            Correo: body.Correo,
        });
        
        //@ts-ignore
        const licensee = Licensee.build({
            Cedula: body.Cedula,
        });

        await professional.save();
        await licensee.save();

        res.json([professional, licensee]);

    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Hable con el administrador",
        });
    }
};

export const deleteLicensee = async (req: Request, res: Response) => {
    const { id } = req.params;

    //@ts-ignoreclear

    const licensee = await query.findById(id);

    if (!licensee) {
        return res.status(200).json({
            msg: `No existe un licenciado con la cédula ${id}`
        });
    }

    await licensee.update({ estado: false });

    await licensee.destroy();

    res.json(licensee);
};