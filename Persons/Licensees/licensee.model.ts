//importamos sequelize y la conexion a la base de datos
import { DataTypes, Model } from "sequelize";
import db from "../../db/connection";

class Licensee extends Model {
  public Cedula!: string;
}

//@ts-ignore
Licensee.init(
  {
    Cedula: {
        type: DataTypes.STRING,
        primaryKey: true,
    },
  },
  {
      //@ts-ignore
      tableName: "Licenciado",
      sequelize: db,
  },
)

//exportamos como default
export default Licensee;