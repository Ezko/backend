import { Router } from "express";
import { requireJwtMiddleware } from "../../middleware/auth.middleware";
import { getLicensees, getLicensee, postLicensee, deleteLicensee } from "./licensee.service";

/**
 * Router Definition
 */

const licenseeRouter = Router();

licenseeRouter.use(requireJwtMiddleware);


licenseeRouter.get("/", getLicensees);
licenseeRouter.get("/:id", getLicensee);
licenseeRouter.post("/", postLicensee);
licenseeRouter.delete("/:id", deleteLicensee);

export default licenseeRouter;