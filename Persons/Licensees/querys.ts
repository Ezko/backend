import Professional from "../Professionals/professional.model";
import Licensee from "./licensee.model";

//@ts-ignore
export const getAll = async () => {
  //@ts-ignore
  const licensees = await Licensee.findAll();
  let retorno = new Array<Professional>();
  for (const licensee of licensees) {
    //@ts-ignore
    const prof = await Professional.findOne({
      where: {
        Cedula: licensee.Cedula,
      },
    });
    if (prof) {
      retorno.push(prof);
    }
  }
  return retorno;
};

//@ts-ignore
export const findById = async (id: number) => await Licensee.findByPk(id);

//@ts-ignore
export const findOne = async (body: any) =>
  //@ts-ignore

  await Licensee.findOne({
    where: {
      Cedula: body.Cedula,
    },
  });
