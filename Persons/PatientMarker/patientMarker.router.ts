import { Router } from "express";
import { requireJwtMiddleware } from "../../middleware/auth.middleware";
import { getPatientMarkers, getPatientMarker, postPatientMarker, putPatientMarker, deletePatientMarker } from "./patientMarker.service";

/**
 * Router Definition
 */

const patientMarkerRouter = Router();

patientMarkerRouter.use(requireJwtMiddleware);


patientMarkerRouter.get("/", getPatientMarkers);
patientMarkerRouter.get("/:documento", getPatientMarker);
patientMarkerRouter.post("/", postPatientMarker);
patientMarkerRouter.put("/:documento/:marcadorViral", putPatientMarker);
patientMarkerRouter.delete("/:documento/:marcadorViral", deletePatientMarker);

export default patientMarkerRouter;