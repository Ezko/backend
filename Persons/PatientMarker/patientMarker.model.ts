//importamos sequelize y la conexion a la base de datos
import { DataTypes, Model } from "sequelize";
import db from "../../db/connection";

class PatientMarker extends Model {
  public Documento!: string;
  public MarcadorViral!: string;
}

//@ts-ignore
PatientMarker.init(
  {
    Documento: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    MarcadorViral: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
  },
  {
    //@ts-ignore
    tableName: "PacienteMarcador",
    sequelize: db,
  }
)

//exportamos como default
export default PatientMarker;