import PatientMarker from "./patientMarker.model";

//@ts-ignore
export const getAll = async () => await PatientMarker.findAll();

//@ts-ignore
export const findOne = async (body: any) => 
    //@ts-ignore
    await PatientMarker.findOne({
        where: {
            Documento: body.Documento,
            MarcadorViral: body.MarcadorViral,
        },
    });

//@ts-ignore
export const findPatientMarkers = async (documento: string) => 
//@ts-ignore

    await PatientMarker.findAll({
        where: {
            Documento: documento,
        },
    });