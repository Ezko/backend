/**
 * Data Model Interfaces
 */
 import bodyParser from "body-parser";
import { Request, Response } from "express";
 import PatientMarker from "./patientMarker.model";
 import * as query from "./querys"

 /**
 * Service Methods
 */

 export const getPatientMarkers = async (req: Request, res: Response) => {
     
    //@ts-ignore

     const markers = await query.getAll();

     res.json({ markers });
 };

 export const getPatientMarker = async (req: Request, res: Response) => {
     const { documento } = req.params;

    //@ts-ignore

    const marker = await query.findPatientMarkers(documento);

    if (marker) {
        res.json(marker);
    } else {
        res.status(200).json({
            msg: `No existe un paciente con el documento ${documento}`
        });
    }
 };

 export const postPatientMarker = async (req: Request, res: Response) => {
    const { body } = req;

    try {
        //@ts-ignore
        const existsDocumento = await query.findOne(body);

        if (existsDocumento) {
            return res.status(400).json({
                msg: "Ya existe un paciente con el documento " + body.Documento,
            });
        }

        //@ts-ignore
        const marker = PatientMarker.build({
            Documento: body.Documento,
            MarcadorViral: body.MarcadorViral,
        });

        await marker.save();

        res.json(marker);

    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Hable con el administrador",
        });
    }
};

export const putPatientMarker = async (req: Request, res: Response) => {
    const { documento, marcadorViral } = req.params;
    const { body } = req;

    try {
        //@ts-ignore
        var marker = await query.findOne(documento, marcadorViral);
        if (!marker) {
            //@ts-ignore
            marker = PatientMarker.build({
                Documento: documento,
                MarcadorViral: marcadorViral,
            });

            await marker.save();

        } else {
            
            await marker.update(body);

        }

        res.json(marker);

    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Hable con el administrador",
        });
    }
};

export const deletePatientMarker = async (req: Request, res: Response) => {
    const { body } = req;

    //@ts-ignoreclear

    const marker = await query.findOne(body);

    if (!marker) {
        return res.status(200).json({
            msg: "No existe un paciente con el documento " + body.Documento,
        });
    }

    await marker.update({ estado: false });

    await marker.destroy();

    res.json(marker);
};