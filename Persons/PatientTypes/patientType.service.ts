/**
 * Data Model Interfaces
 */
 import { Request, Response } from "express";
import { QueryTypes } from "sequelize/dist";
 import PatientType from "./patientType.model";
 import * as query from "./querys";

 /**
 * Service Methods
 */

 export const getPatientTypes = async (req: Request, res: Response) => {
     
    //@ts-ignore

     const types = await query.getAll();

     res.json({ types });
 };

 export const getPatientType = async (req: Request, res: Response) => {
     const { id } = req.params;

     //@ts-ignore

    const type = await query.findById(id);

    if (type) {
        res.json(type);
    } else {
        res.status(200).json({
            msg: `No existe un tipo de paciente con el id ${id}`
        });
    }
};

export const postType = async (req: Request, res: Response) => {
    const { body } = req;

    try {
        //@ts-ignore
        const existsID = await query.findOne(body);

        if (existsID) {
            return res.status(400).json({
                msg: "Ya existe un tipo con el id " + body.IdTipoPaciente,
            });
        }

        //@ts-ignore
        const type = PatientType.build({
            IdTipoPaciente: body.IdTipoPaciente,
            NombreTipoPaciente: body.NombreTipoPaciente,
        });
        
        //@ts-ignore

        await type.save();

        res.json(type);

    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Hable con el administrador",
        });
    }
};

export const putType = async (req: Request, res: Response) => {
    const { id } = req.params;
    const { body } = req;

    try {
        //@ts-ignore
        var type = await query.findById(id);
        if (!type) {
            //@ts-ignore
            type = PatientType.build({
                IdTipoPaciente: body.IdTipoPaciente,
                NombreTipoPaciente: body.NombreTipoPaciente,
            });

            await type.save();

        } else {
            
            await type.update(body);

        }

        res.json(type);

    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Hable con el administrador",
        });
    }
};

export const deleteType = async (req: Request, res: Response) => {
    const { id } = req.params;

    //@ts-ignoreclear

    const type = await query.findById(id);

    if (!type) {
        return res.status(200).json({
            msg: `No existe un tipo de paciente con el id ${id}`
        });
    }

    await type.update({ estado: false });

    await type.destroy();

    res.json(type);
};