import { Router } from "express";
import { requireJwtMiddleware } from "../../middleware/auth.middleware";
import { getPatientTypes, getPatientType, postType, putType, deleteType } from "./patientType.service";

/**
 * Router Definition
 */

const patientTypeRouter = Router();

patientTypeRouter.use(requireJwtMiddleware);


patientTypeRouter.get("/", getPatientTypes);
patientTypeRouter.get("/:id", getPatientType);
patientTypeRouter.post("/", postType);
patientTypeRouter.put("/:id", putType);
patientTypeRouter.delete("/:id", deleteType);

export default patientTypeRouter;