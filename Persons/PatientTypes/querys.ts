import PatientType from "./patientType.model";

//@ts-ignore
export const getAll = async () => await PatientType.findAll();

//@ts-ignore
export const findOne = async (body: any) =>
  //@ts-ignore
  await PatientType.findOne({
    where: {
        IdTipoPaciente: body.IdTipoPaciente,
    },
  });

//@ts-ignore
export const findById = async (id: number) => await PatientType.findByPk(id);