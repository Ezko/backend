//importamos sequelize y la conexion a la base de datos
import { DataTypes, Model } from "sequelize";
import db from "../../db/connection";

class PatientType extends Model {
    public IdTipoPaciente!: number;
    public NombreTipoPaciente!: string;
}

//@ts-ignore
PatientType.init(
    {
        IdTipoPaciente: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        NombreTipoPaciente: {
            type: DataTypes.STRING,
        }
    }, 
    {
        //@ts-ignore
        tableName: "TipoPaciente",
        sequelize: db,
    }
);

//exportamos como default
export default PatientType;