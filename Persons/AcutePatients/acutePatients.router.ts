import { Router } from "express";
import { requireJwtMiddleware } from "../../middleware/auth.middleware";
import { getAcutePatients, getAcutePatient, postAcutePatient, deleteAcutePatient } from "./acutePatients.service";

/**
 * Router Definition
 */

const acutePatientRouter = Router();

acutePatientRouter.use(requireJwtMiddleware);


acutePatientRouter.get("/", getAcutePatients);
acutePatientRouter.get("/:id", getAcutePatient);
acutePatientRouter.post("/", postAcutePatient);
acutePatientRouter.delete("/:id", deleteAcutePatient);

export default acutePatientRouter;