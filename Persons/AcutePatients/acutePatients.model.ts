//importamos sequelize y la conexion a la base de datos
import { DataTypes, Model } from "sequelize";
import db from "../../db/connection";

class AcutePatient extends Model {
  public Documento!: string;
}

//@ts-ignore
AcutePatient.init(
  {
    Documento: {
          type: DataTypes.STRING,
          primaryKey: true,
      },
  },
  {
      //@ts-ignore
      tableName: "Agudo",
      sequelize: db,
  },
)

//exportamos como default
export default AcutePatient;