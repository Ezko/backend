/**
 * Data Model Interfaces
 */
 import { Request, Response } from "express";
import { QueryTypes } from "sequelize/dist";
 import AcutePatient from "./acutePatients.model";
 import * as query from "./querys";
 import * as queryChronicPatients from "../ChronicPatients/querys"
 import Patient from "../Patients/patient.model";
import PatientMarker from "../PatientMarker/patientMarker.model";
import db from "../../db/connection";

 /**
 * Service Methods
 */

 export const getAcutePatients = async (req: Request, res: Response) => {
     
    //@ts-ignore

     const patients = await query.getAll();

     res.json({ patients });
 };

 export const getAcutePatient = async (req: Request, res: Response) => {
     const { id } = req.params;

     //@ts-ignore

    const patient = await query.findById(id);

    if (patient) {
        res.json(patient);
    } else {
        res.status(200).json({
            msg: `No existe un paciente con el documento ${id}`
        });
    }
};

export const postAcutePatient = async (req: Request, res: Response) => {
    const { body } = req;
    let t;
    try {
        //@ts-ignore
        const existsID = await query.findOne(body);
        const existsChronicPatient = await queryChronicPatients.findOne(body);

        if (existsChronicPatient) {
            return res.status(400).json({
                msg: "Un paciente no puede ser crónico y agudo al mismo tiempo.",
            });
        }

        if (existsID) {
            return res.status(400).json({
                msg: "Ya existe un paciente con el documento " + body.Documento,
            });
        }

        //@ts-ignore
        const patient = Patient.build({
            Documento: body.Documento,
            Estado: body.Estado,
            Nombre: body.Nombre,
            Apellido: body.Apellido,
            TelPrincipal: body.TelPrincipal,
            TelSecundario: body.TelSecundario,
            Correo: body.Correo,
        });
        
        //@ts-ignore
        const acutePatient = AcutePatient.build({
            Documento: body.Documento,
        });

        //@ts-ignore
        t = await db.transaction();
        await patient.save(), { t };
        await acutePatient.save(), { t };
        let counter = 0;
        while (counter<body.MarcadoresVirales.length) {
            //@ts-ignores
            await PatientMarker.create({
                Documento: body.Documento,
                MarcadorViral: body.MarcadoresVirales[counter]
            }, { transaction: t })
            counter++;
        }
            
        

        //@ts-ignore
        t.commit();

        res.json([patient, acutePatient]);

    } catch (error) {
        if(t){await t.rollback()}
        console.log(error);
        res.status(500).json({
            msg: "Hable con el administrador",
        });
    }
};

export const deleteAcutePatient = async (req: Request, res: Response) => {
    const { id } = req.params;

    //@ts-ignoreclear

    const patient = await query.findById(id);

    if (!patient) {
        return res.status(200).json({
            msg: `No existe un paciente con el documento ${id}`
        });
    }

    await patient.update({ estado: false });

    await patient.destroy();

    res.json(patient);
};