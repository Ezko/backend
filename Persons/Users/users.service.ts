/**
 * Data Model Interfaces
 */
import { Request, Response } from "express";
import User from "./users.model";
import * as query from "./querys";
import { encodeSession } from "../../middleware/Jwt.middleware";

/**
 * Service Methods
 */

export const getUsers = async (req: Request, res: Response) => {
  //@ts-ignore

  const users = await query.findAll();

  res.json({ users });
};

export const getUser = async (req: Request, res: Response) => {
  const { id } = req.params;

  //@ts-ignore

  const user = await query.findById(id);

  if (user) {
    res.json(user);
  } else {
    res.status(200).json({
      msg: `No existe un usuario con el id ${id}`,
    });
  }
};

export const postUser = async (req: Request, res: Response) => {
  const { body } = req;

  try {
    //@ts-ignore
    const existsID = await query.findOne(body);

    if (existsID) {
      return res.status(400).json({
        msg: "Ya existe un usuario con el ID " + body.ID_USUARIO,
      });
    }

    //@ts-ignore
    const user = User.build({
      ID_USUARIO: body.ID_USUARIO,
      USUARIO: body.USUARIO,
      CLAVE: body.CLAVE,
    });

    await user.save();

    res.json(user);
  } catch (error) {
    console.log(error);
    res.status(500).json({
      msg: "Hable con el administrador",
    });
  }
};

export const putUser = async (req: Request, res: Response) => {
  const { id } = req.params;
  const { body } = req;

  try {
    //@ts-ignore
    var user = await query.findById(id);
    if (!user) {
      //@ts-ignore
      user = User.build({
        ID_USUARIO: body.ID_USUARIO,
        USUARIO: body.USUARIO,
        CLAVE: body.CLAVE,
      });

      await user.save();
    } else {
      await user.update(body);
    }

    res.json(user);
  } catch (error) {
    console.log(error);
    res.status(500).json({
      msg: "Hable con el administrador",
    });
  }
};

export const deleteUser = async (req: Request, res: Response) => {
  const { id } = req.params;

  //@ts-ignoreclear

  const user = await query.findById(id);

  if (!user) {
    return res.status(200).json({
      msg: "No existe un usuario con el ID " + id,
    });
  }

  await user.update({ estado: false });

  await user.destroy();

  res.json(user);
};

export const loginUser = async (req: Request, res: Response) => {
  const { body } = req;
  //@ts-ignore
  const user = await query.findLogin(body.Username, body.Password);
  console.log(user);
  if (user) {
    const session = encodeSession(process.env.JWT_STRING!, {
      id: user.ID_USUARIO,
      username: user.USUARIO,
      dateCreated: Date.now(),
    });
    res.status(200).json(session);
  } else {
    res.status(401).json({
      msg: `Nombre de usuario o contraseña incorrectos`,
    });
  }
};
