import User from "./users.model";

//@ts-ignore
export const findAll = async () => await User.findAll();

//@ts-ignore
export const findOne = async (body: any) => 
    //@ts-ignore
    await User.findOne({
        where: {
            ID_USUARIO: body.ID_USUARIO,
        },
    });

//@ts-ignore
export const findById = async (id: number) => await User.findByPk(id);

//@ts-ignore
export const findLogin = async (username: string, password: string) =>
    //@ts-ignore
    await User.findOne({
        where: {
            USUARIO : username,
            CLAVE : password
        },
    });