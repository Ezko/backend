//importamos sequelize y la conexion a la base de datos
import { DataTypes, Model } from "sequelize";
import db from "../../db/connection";

class User extends Model {
    public ID_USUARIO!: number;
    public USUARIO!: string;
    public CLAVE!: string;
}

//@ts-ignore
User.init(
    {
        ID_USUARIO: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },
        USUARIO: {
            type: DataTypes.STRING,
        },
        CLAVE: {
            type: DataTypes.STRING,
        },
    }, 
    {
        //@ts-ignore
        tableName: "USUARIOS",
        sequelize: db,
    }
);

//exportamos como default
export default User;