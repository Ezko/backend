import { Router } from "express";
import { requireJwtMiddleware } from "../../middleware/auth.middleware";
import { getUsers, getUser, postUser, putUser, deleteUser, loginUser } from "./users.service";

/**
 * Router Definition
 */

const usersRouter = Router();

usersRouter.get("/",requireJwtMiddleware, getUsers);
usersRouter.get("/:id",requireJwtMiddleware, getUser);
usersRouter.post("/", postUser);
usersRouter.post("/login",loginUser)
usersRouter.put("/:id",requireJwtMiddleware,  putUser);
usersRouter.delete("/:id",requireJwtMiddleware, deleteUser);

export default usersRouter;