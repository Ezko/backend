import { Request, Response } from "express";
 import Patient from "./patient.model";
 import * as query from "./querys";

 /**
 * Service Methods
 */

 export const getPatients = async (req: Request, res: Response) => {
     
    //@ts-ignore

     const patients = await query.findAll();

     res.json({ patients });
 };

 export const getPatient = async (req: Request, res: Response) => {
     const { id } = req.params;

     //@ts-ignore

    const patient = await query.findById(id);

    if (patient) {
        res.json(patient);
    } else {
        res.status(200).json({
            msg: `No existe un paciente con el id ${id}`
        });
    }
};
 
export const getPatientTypes = async (req: Request, res: Response) => {
    res.json(['Transitorio', 'Cronico', 'Agudo']);
}

 export const postPatient = async (req: Request, res: Response) => {
     const { body } = req;
     let tienePCR = false;
     let counter = 0;
     while (!tienePCR && counter<body.MarcadoresVirales.length) {
         if (body.MarcadoresVirales[counter] == 4)
             tienePCR = true;
         counter++
     }
     if (!tienePCR) {
         return res.status(500).json({
             msg: "No se admiten pacientes sin PCR"
         })
     }else if (body.Tipo == "Transitorio") {
         res.redirect(307, req.url+'api/transitoryPatients/')
     } else if (body.Tipo == "Cronico") {
         res.redirect(307, req.url+'api/chronicPatients/')
     }else if (body.Tipo == "Agudo") {
         res.redirect(307, req.url+'api/acutePatients/')
     } else {
         return res.status(400).json({
             msg: "Tipo de paciente invalido"
         })
     }
    // try {
    //     //@ts-ignore
    //     const existsDocumento = await query.findOne(body);

    //     if (existsDocumento) {
    //         return res.status(400).json({
    //             msg: "Ya existe un paciente con el codigo " + body.Documento,
    //         });
    //     }

    //     //@ts-ignore
    //     const patient = Patient.build({
    //         Documento: body.Documento,
    //         Estado: body.Estado,
    //         Nombre: body.Nombre,
    //         Apellido: body.Apellido,
    //         TelPrincipal: body.TelPrincipal,
    //         TelSecundario: body.TelSecundario,
    //         PCR: body.PCR,
    //         Correo: body.Correo,
    //     });

    //     await patient.save();

    //     res.json(patient);

    // } catch (error) {
    //     console.log(error);
    //     res.status(500).json({
    //         msg: "Hable con el administrador",
    //     });
    // }
};

export const putPatient = async (req: Request, res: Response) => {
    const { id } = req.params;
    const { body } = req;

    try {
        //@ts-ignore
        var patient = await query.findById(id);
        if (!patient) {
            //@ts-ignore
            patient = Patient.build({
                Documento: body.Documento,
                Estado: body.Estado,
                Nombre: body.Nombre,
                Apellido: body.Apellido,
                TelPrincipal: body.TelPrincipal,
                TelSecundario: body.TelSecundario,
                PCR: body.PCR,
                Correo: body.Correo,
            });

            await patient.save();

        } else {
            
            await patient.update(body);

        }

        res.json(patient);

    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Hable con el administrador",
        });
    }
};

export const deletePatient = async (req: Request, res: Response) => {
    const { id } = req.params;

    //@ts-ignoreclear

    const patient = await query.findById(id);

    if (!patient) {
        return res.status(200).json({
            msg: "No existe una maquina con el codigo " + id,
        });
    }

    await patient.update({ estado: false });

    await patient.destroy();

    res.json(patient);
};