import { Router } from "express";
import { requireJwtMiddleware } from "../../middleware/auth.middleware";
import { getPatient, getPatients, postPatient, putPatient, deletePatient, getPatientTypes } from "./patient.service";

/**
 * Router Definition
 */

const patientsRouter = Router();

patientsRouter.use(requireJwtMiddleware);


patientsRouter.get("/", getPatients);
patientsRouter.get("/:id", getPatient);
patientsRouter.get("/types/getTypes", getPatientTypes);
patientsRouter.post("/", postPatient);
patientsRouter.put("/:id", putPatient);
patientsRouter.delete("/:id", deletePatient);

export default patientsRouter;