//importamos sequelize y la conexion a la base de datos
import { DataTypes, Model } from "sequelize";
import db from "../../db/connection";

class Patient extends Model {
    public Documento!: string;
    public Estado!: string;
    public Nombre!: string;
    public Apellido!: string;
    public TelPrincipal!: string;
    public TelSecundario!: string;
    public Correo!: string;
}

//@ts-ignore
Patient.init(
    {
        Documento: {
            type: DataTypes.STRING,
            primaryKey: true
        },
        Estado: {
            type: DataTypes.STRING,
        },
        Nombre: {
            type: DataTypes.STRING,
        },
        Apellido: {
            type: DataTypes.STRING,
        },
        TelPrincipal: {
            type: DataTypes.STRING,
        },
        TelSecundario: {
            type: DataTypes.STRING,
        },
        Correo: {
            type: DataTypes.STRING,
        },
    }, 
    {
        //@ts-ignore
        tableName: "Paciente",
        sequelize: db,
    }
);

//exportamos como default
export default Patient;