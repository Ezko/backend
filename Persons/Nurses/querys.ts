import Professional from "../Professionals/professional.model";
import Nurse from "./nurse.model";

//@ts-ignore
export const findAll = async () => {
  //@ts-ignore
  const nurses = await Nurse.findAll();
  let retorno = new Array<Professional>();
  for (const nurse of nurses) {
    //@ts-ignore
    const prof = await Professional.findOne({
      where: {
        Cedula: nurse.Cedula,
      },
    });
    if (prof) {
      retorno.push(prof);
    }
  }
  return retorno;
};

//@ts-ignore
export const findOne = async (id: any) => {
  //@ts-ignore

  const nurse = await Nurse.findOne({
    where: {
      Cedula: id,
    },
  });

  if (nurse) {
    //@ts-ignore
    return await Professional.findOne({
      where: {
        Cedula: id,
      },
    });
  }
};
//@ts-ignore
export const findById = async (id: number) => await Nurse.findByPk(id);
