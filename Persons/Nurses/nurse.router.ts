import { Router } from "express";
import { requireJwtMiddleware } from "../../middleware/auth.middleware";
import { getNurses, getNurse, postNurse, putNurse, deleteNurse } from "./nurse.service";

/**
 * Router Definition
 */

const nurseRouter = Router();

nurseRouter.use(requireJwtMiddleware);


nurseRouter.get("/", getNurses);
nurseRouter.get("/:id", getNurse);
nurseRouter.post("/", postNurse);
nurseRouter.put("/:id", putNurse);
nurseRouter.delete("/:id", deleteNurse);

export default nurseRouter;