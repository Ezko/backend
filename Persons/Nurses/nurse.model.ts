//importamos sequelize y la conexion a la base de datos
import { DataTypes, Model } from "sequelize";
import db from "../../db/connection";

class Nurse extends Model {
  public Cedula!: string;
}

//@ts-ignore
Nurse.init(
  {
    Cedula: {
      type: DataTypes.STRING,
      primaryKey: true,
    }
  },
  {
    //@ts-ignore
    tableName: "Enfermero",
    sequelize: db,
  }
)

//exportamos como default
export default Nurse;