/**
 * Data Model Interfaces
 */
import { Request, Response } from "express";
import Nurse from "./nurse.model";
import * as query from "./querys";
import Professional from "../Professionals/professional.model";

 /**
 * Service Methods
 */

 export const getNurses = async (req: Request, res: Response) => {
     
    //@ts-ignore

     const nurses = await query.findAll();

     res.json({ nurses });
 };

 export const getNurse = async (req: Request, res: Response) => {
     const { id } = req.params;

     //@ts-ignore

    const nurse = await query.findOne(id);

    if (nurse) {
        res.json(nurse);
    } else {
        res.status(200).json({
            msg: `No existe un enfermero con la cédula ${id}`
        });
    }
 };

 export const postNurse = async (req: Request, res: Response) => {
    const { body } = req;

    try {
        //@ts-ignore
        const existsCedula = await query.findOne(body);

        if (existsCedula) {
            return res.status(400).json({
                msg: "Ya existe un enfermero con la cédula " + body.Cedula,
            });
        }

        //@ts-ignore
        const professional = Professional.build({
            Cedula: body.Cedula,
            Nombre: body.Nombre,
            Apellido: body.Apellido,
            Telefono: body.Telefono,
            Estado: body.Estado,
            Correo: body.Correo,
        });

        //@ts-ignore
        const nurse = Nurse.build({
            Cedula: body.Cedula,
        });

        await professional.save();
        await nurse.save();

        res.json([professional, nurse]);

    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Hable con el administrador",
        });
    }
};

export const putNurse = async (req: Request, res: Response) => {
    const { id } = req.params;
    const { body } = req;

    try {
        //@ts-ignore
        var nurse = await query.findById(id);
        if (!nurse) {
            //@ts-ignore
            nurse = Nurse.build({
                Cedula: body.Cedula,
            });

            await nurse.save();

        } else {
            
            await nurse.update(body);

        }

        res.json(nurse);

    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Hable con el administrador",
        });
    }
};

export const deleteNurse = async (req: Request, res: Response) => {
    const { id } = req.params;

    //@ts-ignoreclear

    const nurse = await query.findById(id);

    if (!nurse) {
        return res.status(200).json({
            msg: "No existe un enfermero con el código " + id,
        });
    }

    await nurse.update({ estado: false });

    await nurse.destroy();

    res.json(nurse);
};
