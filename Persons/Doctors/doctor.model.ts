//importamos sequelize y la conexion a la base de datos
import { DataTypes, Model } from "sequelize";
import db from "../../db/connection";

//definimos una const reflejando los datos de la tabla en la BD

class Doctor extends Model {
  public Cedula!: string;
}

//@ts-ignore
Doctor.init(
  {
    Cedula: {
      type: DataTypes.STRING,
      primaryKey: true,
    }
  },
  {
    //@ts-ignore
    tableName: "Medico",
    sequelize: db,
  }
)

//exportamos como default
export default Doctor;