import Professional from "../Professionals/professional.model";
import Doctor from "./doctor.model";

//@ts-ignore
export const findAll = async () => {
  //@ts-ignore
  const doctors = await Doctor.findAll();
  let retorno = new Array<Professional>();
  for (const doctor of doctors) {
    //@ts-ignore
    const prof = await Professional.findOne({
      where: {
        Cedula: doctor.Cedula,
      },
    });
    if (prof) {
      retorno.push(prof);
    }
  }
  return retorno;
};

//@ts-ignore
export const findOne = async (body: any) =>
  //@ts-ignore
  await Doctor.findOne({
    where: {
      Cedula: body.Cedula,
    },
  });

//@ts-ignore
export const findById = async (id: number) => await Doctor.findByPk(id);
