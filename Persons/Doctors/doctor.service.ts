/**
 * Data Model Interfaces
 */
import bodyParser from "body-parser";
import { Request, Response } from "express";
import Doctor from "./doctor.model";
import * as query from "./querys";
import Professional from "../Professionals/professional.model";

 /**
 * Service Methods
 */

 export const getDoctors = async (req: Request, res: Response) => {
     
    //@ts-ignore

     const doctors = await query.findAll();

     res.json({ doctors });
 };

 export const getDoctor = async (req: Request, res: Response) => {
     const { id } = req.params;

     //@ts-ignore

    const doctor = await query.findById(id);

    if (doctor) {
        res.json(doctor);
    } else {
        res.status(200).json({
            msg: `No existe un doctor con la cédula ${id}`
        });
    }
 };

 export const postDoctor = async (req: Request, res: Response) => {
    const { body } = req;

    try {
        //@ts-ignore
        const existsCedula = await query.findOne(body);

        if (existsCedula) {
            return res.status(400).json({
                msg: "Ya existe un médico con la cédula " + body.Cedula,
            });
        }

        //@ts-ignore
        const professional = Professional.build({
            Cedula: body.Cedula,
            Nombre: body.Nombre,
            Apellido: body.Apellido,
            Telefono: body.Telefono,
            Estado: body.Estado,
            Correo: body.Correo,
        });
        
        //@ts-ignore
        const doctor = Doctor.build({
            Cedula: body.Cedula,
        });

        await professional.save();
        await doctor.save();

        res.json([professional, doctor]);

    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Hable con el administrador",
        });
    }
};

export const putDoctor = async (req: Request, res: Response) => {
    const { id } = req.params;
    const { body } = req;

    try {
        //@ts-ignore
        var doctor = await query.findById(id);
        if (!doctor) {
            //@ts-ignore
            doctor = Doctor.build({
                Cedula: body.Cedula,
            });

            await doctor.save();

        } else {
            
            await doctor.update(body);

        }

        res.json(doctor);

    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Hable con el administrador",
        });
    }
};

export const deleteDoctor = async (req: Request, res: Response) => {
    const { id } = req.params;

    //@ts-ignoreclear

    const doctor = await query.findById(id);

    if (!doctor) {
        return res.status(200).json({
            msg: "No existe un médico con el código " + id,
        });
    }

    await doctor.update({ estado: false });

    await doctor.destroy();

    res.json(doctor);
};