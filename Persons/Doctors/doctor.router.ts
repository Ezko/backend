import { Router } from "express";
import { requireJwtMiddleware } from "../../middleware/auth.middleware";
import { getDoctors, getDoctor, postDoctor, putDoctor, deleteDoctor } from "./doctor.service";

/**
 * Router Definition
 */

const doctorsRouter = Router();

doctorsRouter.use(requireJwtMiddleware);


doctorsRouter.get("/", getDoctors);
doctorsRouter.get("/:id", getDoctor);
doctorsRouter.post("/", postDoctor);
doctorsRouter.put("/:id", putDoctor);
doctorsRouter.delete("/:id", deleteDoctor);

export default doctorsRouter;