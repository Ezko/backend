/**
 * Required External Modules
 */
import express, { Application } from "express";
import helmet from "helmet";
import cors from "cors";
import roomsRouter from "./Infrastructure/Rooms/rooms.router";
import machinesRouter from "./Infrastructure/Machines/machines.router";
import db from "./db/connection";
import valveRouter from "./Infrastructure/Valves/valve.router";
import patientsRouter from "./Persons/Patients/patient.router";
import dialysisRouter from "./Coordination/Dialysis/dialysis.router";
import nurseRouter from "./Persons/Nurses/nurse.router";
import doctorsRouter from "./Persons/Doctors/doctor.router";
import patientMarkerRouter from "./Persons/PatientMarker/patientMarker.router";
import professionalsRouter from "./Persons/Professionals/professional.router";
import shiftsRouter from "./Coordination/Shift/shift.router";
import shiftProfRouter from "./Coordination/ShiftProf/shiftProf.router";
import usersRouter from "./Persons/Users/users.router";
import licenseeRouter from "./Persons/Licensees/licensee.router";
import acutePatientRouter from "./Persons/AcutePatients/acutePatients.router";
import chronicPatientRouter from "./Persons/ChronicPatients/chronicPatient.router";
import transitoryPatientRouter from "./Persons/TransitoryPatients/transitoryPatient.router";
import rsRouter from "./Coordination/RoomShift/roomshifts.router";
import viralMarkerRouter from "./ViralMarkers/viralMarkers.router";
import patientTypeRouter from "./Persons/PatientTypes/patientType.router";

class Server {
  private app: Application;
  private port: string;
  private apiPaths = {
    rooms: "/api/rooms",
    machines: "/api/machines",
    valves: "/api/valve",
    patients: "/api/patients",
    dialysis: "/api/dialysis",
    nurses: "/api/nurses",
    doctors: "/api/doctors",
    patientMarkers: "/api/patientMarkers",
    professionals: "/api/professionals",
    shifts: "/api/shifts",
    shiftProfs: "/api/shiftProfs",
    users: "/api/users",
    licensees: "/api/licensees",
    acutePatients: "/api/acutePatients",
    chronicPatients: "/api/chronicPatients",
    transitoryPatients: "/api/transitoryPatients",
    roomShift: "/api/roomShift",
    viralMarker: "/api/viralMarker",
    patientType: "/api/patientTypes",
  };

  constructor() {
    this.app = express();
    this.port = process.env.PORT || "3000";
    this.dbConnection();
    this.middlewares();
    this.routes();
  }

  async dbConnection() {
    try {
      await db.authenticate();
      console.log("connected succesfully");
    } catch (err) {
      if (err instanceof Error) throw err;
    }
  }

  middlewares() {
    this.app.use(cors());

    this.app.use(helmet());

    this.app.use(express.json());
  }

  routes() {
    this.app.use(this.apiPaths.rooms, roomsRouter);
    this.app.use(this.apiPaths.machines, machinesRouter);
    this.app.use(this.apiPaths.valves, valveRouter);
    this.app.use(this.apiPaths.patients, patientsRouter);
    this.app.use(this.apiPaths.dialysis, dialysisRouter);
    this.app.use(this.apiPaths.nurses, nurseRouter);
    this.app.use(this.apiPaths.doctors, doctorsRouter);
    this.app.use(this.apiPaths.patientMarkers, patientMarkerRouter);
    this.app.use(this.apiPaths.professionals, professionalsRouter);
    this.app.use(this.apiPaths.shifts, shiftsRouter);
    this.app.use(this.apiPaths.shiftProfs, shiftProfRouter);
    this.app.use(this.apiPaths.users, usersRouter);
    this.app.use(this.apiPaths.licensees, licenseeRouter);
    this.app.use(this.apiPaths.acutePatients, acutePatientRouter);
    this.app.use(this.apiPaths.chronicPatients, chronicPatientRouter);
    this.app.use(this.apiPaths.transitoryPatients, transitoryPatientRouter);
    this.app.use(this.apiPaths.roomShift, rsRouter);
    this.app.use(this.apiPaths.viralMarker, viralMarkerRouter);
    this.app.use(this.apiPaths.patientType, patientTypeRouter);

    console.log(this.apiPaths);
  }

  listen() {
    this.app.listen(this.port, () => {
      console.log("Servidor corriendo en puerto " + this.port);
    });
  }
}

export default Server;
