import { DataTypes, Model } from "sequelize";
import db from "../db/connection";

class Config extends Model {
    public ConfigurationName!: string;
    public Value!: string;
}

Config.init(
    {
        ConfigurationName: {
            type: DataTypes.STRING,
            primaryKey: true,
        },
        Value: {
            type: DataTypes.STRING,
        },
    }, 
    {
        //@ts-ignore
        tableName: "Config",
        sequelize: db,
    }
);

//exportamos como default
export default Config;