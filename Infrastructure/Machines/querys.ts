import Machine from "./machine.model";
import Valve from "../Valves/valve.model";


//@ts-ignore
export const getAll = async () => await Machine.findAll();

//@ts-ignore
export const findOne = async (body: any) => 
    //@ts-ignore
    await Machine.findOne({
        where: {
            CodMaquina: body.CodMaquina,
        },
    });

//@ts-ignore
export const getById = async (CodMaquina: number, CodPico: number, FechaConexion: number) =>
  //@ts-ignore
  await Machine.findOne({ where: { CodMaquina, CodPico, FechaConexion } });

export const getFromPicos =async (picos: Valve[]) => {
    let ret = new Array<Machine>();
    //@ts-ignore
    for (const pico of picos){
        //@ts-ignore
        const m=await Machine.findOne({
            where: {
                CodPico: pico.CodPico
            }
        })
        if (m) {
            console.log(ret.push(m))
        }
    };
    return ret;
}
