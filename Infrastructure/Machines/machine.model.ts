import { DataTypes, Model } from "sequelize";
import db from "../../db/connection";

class Machine extends Model {
  public CodMaquina!: number;
  public CodPico!: number;
  public FechaConexion!: Date;
}

//@ts-ignore
Machine.init(
  {
    CodMaquina: {
      type: DataTypes.BIGINT,
      primaryKey: true,
    },
    CodPico: {
      type: DataTypes.BIGINT,
      primaryKey: true,
    },
    FechaConexion: {
      type: DataTypes.DATE,
      primaryKey: true,
    },
  },
  {
    //@ts-ignore
    tableName: "Maquina",
    sequelize: db,
  }
);

export default Machine;
