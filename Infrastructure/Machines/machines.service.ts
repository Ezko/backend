/**
 * Data Model Interfaces
 */
import { Request, Response } from "express";
import Machine from "./machine.model";
import * as query from "./querys";
import * as queryRooms from "../Rooms/querys";
import * as queryValves from "../Valves/querys";

/**
 * Service Methods
 */

export const getMachines = async (req: Request, res: Response) => {
  //@ts-ignore
  const machines = await query.getAll();

  res.json({ machines });
};

export const getMachine = async (req: Request, res: Response) => {
  const { codMaquina, codPico, fechaConexion } = req.params;

  //@ts-ignore
  const machine = await query.getById(codMaquina, codPico, fechaConexion);

  if (machine) {
    res.json(machine);
  } else {
    res.status(200).json({
      msg: `No existe una máquina con el id ${codMaquina} ${codPico} ${fechaConexion}`,
    });
  }
};

export const getMachinesByRoom = async (req: Request, res: Response) => { 
  const { codSala } = req.params;

  const sala = await queryRooms.findById(parseInt(codSala));

  if(sala){
    const picos = await queryValves.getByRoom(parseInt(sala.CodSala));
    const ret = await query.getFromPicos(picos);
    return res.json(ret);
  }
}

export const postMachine = async (req: Request, res: Response) => {
  const { body } = req;

  try {
    //@ts-ignore
    const existsMaquina = await query.getById(body.CodMaquina, body.CodPico, body.FechaConexion);

    if (existsMaquina) {
      return res.status(400).json({
        msg: `No existe una máquina con el id ${body.CodMaquina} ${body.CodPico} ${body.FechaConexion}`,
      });
    }

    //@ts-ignore
    const machine = Machine.build({
      CodMaquina: body.CodMaquina,
      CodPico: body.CodPico,
      FechaConexion: body.FechaConexion,
    });
    await machine.save();

    res.json(machine);
  } catch (error) {
    console.log(error);
    res.status(500).json({
      msg: "Hable con el administrador",
    });
  }
};

export const patchMachine = async (req: Request, res: Response) => {
  const { id } = req.params;
  const { body } = req;

  try {
    //@ts-ignore
    const machine = await Machine.findByPk(id);
    if (!machine) {
      return res.status(200).json({
        msg: "No existe una maquina con el codigo " + id,
      });
    }

    await machine.update(body);

    res.json(machine);
  } catch (error) {
    console.log(error);
    res.status(500).json({
      msg: "Hable con el administrador",
    });
  }
};

export const deleteMachine = async (req: Request, res: Response) => {
  const { codMaquina, codPico, fechaConexion } = req.params;
  //@ts-ignoreclear
  const machine = await query.getById(codMaquina, codPico, fechaConexion);
  if (!machine) {
    return res.status(200).json({
      msg: `No existe una máquina con el id ${codMaquina} ${codPico} ${fechaConexion}`,
    });
  }

  await machine.update({ estado: false });

  await machine.destroy();

  res.json(machine);
};
