/**
 * Required External Modules and Interfaces
 */

import { Router } from "express";
import { requireJwtMiddleware } from "../../middleware/auth.middleware";
import { deleteMachine, getMachine, getMachines, patchMachine, postMachine, getMachinesByRoom } from "./machines.service";

/**
 * Router Definition
 */

const machinesRouter = Router();

machinesRouter.use(requireJwtMiddleware);


// routing of functions getAll, getOne, post, put, and delete
machinesRouter.get("/", getMachines);
machinesRouter.get("/:codMaquina/:codPico/:fechaConexion", getMachine);
machinesRouter.get("/:codSala", getMachinesByRoom)
machinesRouter.post("/", postMachine);
machinesRouter.patch("/:id", patchMachine);
machinesRouter.delete("/:codMaquina/:codPico/:fechaConexion", deleteMachine);

export default machinesRouter;