/**
 * Data Model Interfaces
 */
import { Request, Response } from "express";
import Room from "./room.model";
import * as query from "./querys";

/**
 * Service Methods
 */

export const getRooms = async (req: Request, res: Response) => {
  //@ts-ignore
  const rooms = await query.getAll();

  res.json({ rooms });
};

export const getRoom = async (req: Request, res: Response) => {
  const { id } = req.params;

  //@ts-ignore
  const room = await query.findById(id);

  if (room) {
    res.json(room);
  } else {
    res.status(200).json({
      msg: `No existe una sala con el id ${id}`,
    });
  }
};

export const postRoom = async (req: Request, res: Response) => {
  const { body } = req;

  try {
    //@ts-ignore
    const existsCodSala = await query.findOne(body);

    if (existsCodSala) {
      return res.status(400).json({
        msg: "Ya existe una sala con el codigo " + body.CodSala,
      });
    }

    //@ts-ignore
    const room = Room.build({
      CodSala: body.CodSala,
      Capacidad: body.Capacidad,
      Nombre: body.Nombre,
    });
    await room.save();

    res.json(room);
  } catch (error) {
    console.log(error);
    res.status(500).json({
      msg: "Hable con el administrador",
    });
  }
};

export const putRoom = async (req: Request, res: Response) => {
  const { id } = req.params;
  const { body } = req;

  try {
    //@ts-ignore
    var room = await query.findById(id);
    if (!room) {
      //@ts-ignore
      room = Room.build({
        CodSala: body.CodSala,
        Capacidad: body.Capacidad,
        Nombre: body.Nombre,
      });
      await room.save();
    } else {
      await room.update(body);
    }

    res.json(room);
  } catch (error) {
    console.log(error);
    res.status(500).json({
      msg: "Hable con el administrador",
    });
  }
};

export const patchRoom = async (req: Request, res: Response) => {
  const { id } = req.params;
  const { body } = req;

  try {
    //@ts-ignore
    const room = await Room.findById(id);
    if (!room) {
      return res.status(404).json({
        msg: "No existe una sala con el codigo " + id,
      });
    }

    await room.update(body);

    res.json(room);
  } catch (error) {
    console.log(error);
    res.status(500).json({
      msg: "Hable con el administrador",
    });
  }
};

export const deleteRoom = async (req: Request, res: Response) => {
  const { id } = req.params;

  //@ts-ignore
  const room = await query.findById(id);
  if (!room) {
    return res.status(404).json({
      msg: "No existe una sala con el codigo " + id,
    });
  }

  await room.update({ estado: false });

  await room.destroy();

  res.json(room);
};

export const getRoomsAvailability = async (req: Request, res: Response) => {
  const { codTurno, fecha } = req.params;
  //@ts-ignore
  res.json(await query.roomAvailability(+codTurno, fecha));
};
