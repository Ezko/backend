/**
 * Required External Modules and Interfaces
 */

import { Router } from "express";
import { requireJwtMiddleware } from "../../middleware/auth.middleware";
import {
  deleteRoom,
  getRoom,
  getRooms,
  getRoomsAvailability,
  patchRoom,
  postRoom,
  putRoom,
} from "./rooms.service";

/**
 * Router Definition
 */

const roomsRouter = Router();

roomsRouter.use(requireJwtMiddleware);


// routing of functions getAll, getOne, post, put, and delete
roomsRouter.get("/", getRooms);
roomsRouter.get("/:id", getRoom);
roomsRouter.get("/getAvailability/:codTurno/:fecha", getRoomsAvailability);
roomsRouter.post("/", postRoom);
roomsRouter.put("/:id", putRoom);
roomsRouter.patch("/:id", patchRoom);
roomsRouter.delete("/:id", deleteRoom);

export default roomsRouter;
