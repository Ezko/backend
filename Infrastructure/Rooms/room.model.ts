//importamos sequelize y la conexion a la base de datos
import { Model, DataTypes } from "sequelize";
import db from "../../db/connection";

//definimos una const reflejando los datos de la tabla en la BD
class Room extends Model {
  public CodSala!: string;
  public Capacidad!: number;
  public Nombre!: string;
}

//@ts-ignore
Room.init(
  {
    CodSala: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    Capacidad: {
      type: DataTypes.NUMBER,
    },
    Nombre: {
      type: DataTypes.STRING,
    },
  },
  {
    //@ts-ignore
    tableName: "Sala",
    sequelize: db,
  }
);

//exportamos como default
export default Room;
