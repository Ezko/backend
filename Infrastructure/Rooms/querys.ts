import Room from "./room.model";
import db from "../../db/connection";
import * as query from "./querys";

//@ts-ignore
export const getAll = async () => await Room.findAll();

//@ts-ignore
export const findOne = async (body: any) =>
  //@ts-ignore
  await Room.findOne({
    where: {
      CodSala: body.CodSala,
    },
  });

//@ts-ignore
export const findById = async (id: number) => await Room.findByPk(id);

export const roomAvailability = async (codTurno: number, fecha: number) => {
  type dispRoomType = {
    CodSala: string;
    Capacidad: number;
    PlazasOcupadas: number;
  };

  class DispRoomClass {
    roomId: string;
    status: string;
    constructor(roomId: string, capacity?: number, occupancy?: number) {
      this.roomId = roomId;
      if (!capacity || !occupancy) {
        this.status = "EMPTY";
      } else if (occupancy > 0 && occupancy < capacity) {
        this.status = "PARTIAL";
      } else if (occupancy >= capacity) {
        this.status = "FULL";
      } else {
        this.status = "EMPTY";
      }
    }
  }

  //@ts-ignore
  const rooms = await query.getAll();

  let ret: DispRoomClass[] = new Array<DispRoomClass>();

  //@ts-ignore
  rooms.forEach((element) => {
    ret.push(new DispRoomClass(element.CodSala));
  });

  //@ts-ignore
  const results = (await db.query("CALL TurnoSalaEstado(:fecha, :codTurno)", {
    replacements: { codTurno: codTurno, fecha: fecha },
  })) as Array<dispRoomType>;
  results.forEach((element) => {
    const transform = new DispRoomClass(
      element.CodSala,
      element.Capacidad,
      element.PlazasOcupadas
    );
    ret.forEach((e) => {
      if (e.roomId === transform.roomId) {
        ret[ret.indexOf(e)] = transform;
      }
    });
  });
  return ret;
};
