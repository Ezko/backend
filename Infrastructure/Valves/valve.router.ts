/**
 * Required External Modules and Interfaces
 */

import { Router } from "express";
import { requireJwtMiddleware } from "../../middleware/auth.middleware";
import {
  deleteValve,
  getValve,
  getValveByRoom,
  getValves,
  postValve,
} from "./valve.service";

/**
 * Router Definition
 */

const valveRouter = Router();

valveRouter.use(requireJwtMiddleware);


// routing of functions getAll, getOne, post, put, and delete
valveRouter.get("/", getValves);
valveRouter.get("/:codPico/:codSala", getValve);
valveRouter.post("/", postValve);
// valveRouter.put("/:id", putValve);
// valveRouter.patch("/:id", patchValve);
valveRouter.delete("/:codPico/:codSala", deleteValve);
valveRouter.get("/:codSala", getValveByRoom);

export default valveRouter;
