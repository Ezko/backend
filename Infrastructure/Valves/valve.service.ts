/**
 * Data Model Interfaces
 */
import { Request, Response } from "express";
import * as query from "./querys";

/**
 * Service Methods
 */

export const getValves = async (req: Request, res: Response) => {
  //@ts-ignore
  const valves = await query.getAll();
  res.json({ valves });
};

export const getValve = async (req: Request, res: Response) => {
  const { codPico, codSala } = req.params;

  //@ts-ignore
  const valve = await query.getById(+codPico, +codSala);

  if (valve) {
    res.json(valve);
  } else {
    res.status(200).json({
      msg: `No existe pico con el id ${codPico} ${codSala}`,
    });
  }
};

export const postValve = async (req: Request, res: Response) => {
  const { body } = req;

  try {
    //@ts-ignore
    const existsCodPico = await query.getById(body.CodPico, body.CodSala);

    if (existsCodPico) {
      return res.status(400).json({
        msg: `Ya existe pico con el codigo ${body.CodSala} ${body.CodPico}`,
      });
    }

    //@ts-ignore
    const valve = await query.insertOne(body.CodPico, body.CodSala);

    res.json(valve);
  } catch (error) {
    console.log(error);
    res.status(500).json({
      msg: "Hable con el administrador",
    });
  }
};

// export const putValve = async (req: Request, res: Response) => {
//   const { id } = req.params;
//   const { body } = req;

//   try {
//     var valve = await Valve.findByPk(id);
//     if (!valve) {
//       valve = Valve.build({
//         CodSala: body.CodSala,
//         CodPico: body.CodPico,
//       });
//       await valve.save();
//     } else {
//       await valve.update(body);
//     }

//     res.json(valve);
//   } catch (error) {
//     console.log(error);
//     res.status(500).json({
//       msg: "Hable con el administrador",
//     });
//   }
// };

// export const patchValve = async (req: Request, res: Response) => {
//   const { id } = req.params;
//   const { body } = req;

//   try {
//     const valve = await Valve.findByPk(id);
//     if (!valve) {
//       return res.status(404).json({
//         msg: "No existe una sala con el codigo " + id,
//       });
//     }

//     await valve.update(body);

//     res.json(valve);
//   } catch (error) {
//     console.log(error);
//     res.status(500).json({
//       msg: "Hable con el administrador",
//     });
//   }
// };

export const deleteValve = async (req: Request, res: Response) => {
  const { codPico, codSala } = req.params;

  //@ts-ignore
  const valve = await query.getById(+codPico, +codSala);
  if (!valve) {
    return res.status(404).json({
      msg: "No existe una sala con el codigo ",
    });
  }
  await valve.update({ estado: false });

  await valve.destroy();

  res.json(valve);
};

export const getValveByRoom = async (req: Request, res: Response) => {
  const { codSala } = req.params;

  console.log(req.params);

  //@ts-ignore
  const valve = await query.getByRoom(+codSala);

  if (valve) {
    res.json(valve);
  } else {
    res.status(200).json({
      msg: `No existen picos en la sala ${codSala}`,
    });
  }
};
