//importamos sequelize y la conexion a la base de datos
import { DataTypes, Model } from "sequelize";
import db from "../../db/connection";
import Room from "../Rooms/room.model";

//definimos una const reflejando los datos de la tabla en la BD

class Valve extends Model {
  public CodPico!: string;
  public CodSala!: number;
}

//@ts-ignore
Valve.init(
  {
    CodPico: {
      type: DataTypes.BIGINT,
      primaryKey: true,
    },
    CodSala: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      references: {
        model: Room,
        key: "CodSala",
      },
    },
  },
  {
    //@ts-ignore
    tableName: "Pico",
    sequelize: db,
  }
);

//exportamos como default
export default Valve;
