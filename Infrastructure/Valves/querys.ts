import Valve from "./valve.model";

export const insertOne = async (CodPico: number, CodSala: number) => {
  //@ts-ignore
  const valve = Valve.build({
    CodSala,
    CodPico,
  });
  await valve.save();

  return valve;
};
//@ts-ignore
export const getAll = async () => await Valve.findAll();

export const getById = async (CodPico: number, CodSala: number) =>
  //@ts-ignore
  await Valve.findOne({ where: { CodPico, CodSala } });

export const getByRoom = async (CodSala: number) =>
  //@ts-ignore
  await Valve.findAll({ where: { CodSala } });
