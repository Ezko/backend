import nodemailer from "nodemailer";
import Config from "../../common/config.model";

export const smtpTransport = nodemailer.createTransport({
    //@ts-ignore
    host: Config.findByPk("DB_HOST").then((res) => {return res}),
    port: 465,
    secure: true, // use SSL
    auth: {
        //@ts-ignore
        user: Config.findByPk("MAILER_USER").then((res) => {return res}),//process.env.MAILER_USER,
        //@ts-ignore
        pass: Config.findByPk("MAILER_PASS").then((res) => {return res}),//process.env.MAILER_PASS
    },
    logger: false
});
