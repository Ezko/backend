import { smtpTransport } from "./transport";
import { scheduleJob } from "node-schedule";
import db from "../../db/connection";
import Professional from "../../Persons/Professionals/professional.model";
import Patient from "../../Persons/Patients/patient.model";
import Shift from "../Shift/shift.model";
import * as queryShift from "../Shift/querys";
import * as queryProf from "../ShiftProf/querys"
import ShiftProf from "../ShiftProf/shiftProf.model";
import Config from "../../common/config.model";




//cuando se guarda la coordinacion, se crean dos pilas de jobs
//Una envia emails a los trabajadores listando los turnos que les toca este mes
//La otra envia a los pacientes el dia antes que les toca tratarse
//la cantidad de mails que cada pila envia por hora es parametrizable
//por ahora ambos envian un maximocombinado de 500 mails por dia(por limites de Gmail)
//si el volumen es demasiado se maneja la posibilidad de usar mas de una cuenta de gmail

//#region email batch scheduler

//crea las pilas de jobs
export const createJobs = async () => {
    let listProfCurrentMonth = await listProf();

    //Job de notificacion de personal
    const jobNotifyProf = scheduleJob('0 12/1 1 * *', async function () {
        scheduleEmailsPersonnel(listProfCurrentMonth);
    })

    let listPatTmrw = await listPat();

    //Job de notificacion de pacientes
    const jobNotifyPat = scheduleJob('0 12 * * *', async function () {
        scheduleEmailsPatients(listPatTmrw);
        
    })
}

//#endregion

//#region ID List Builders

//crea un array de IDs de profesionales que trabajan este mes
const listProf = async () => {
    const fecha = new Date();

    const anio = fecha.getFullYear();

    const mes = fecha.getMonth()+1;

    type ProfID = {
    Cedula : string
    }

    const list = await db.query("CALL professionalIDs(:anio, :mes)", { replacements: { anio: anio, mes: mes } })as Array<ProfID>;
    
    const ret = new Array<string>();

    list.forEach(element => {
        ret.push(element.Cedula)
    });
    
    return ret;
}

//crea un array de IDs de pacientes que se tratan mañana
const listPat = async () => {
    const fecha = new Date();

    const anio = fecha.getFullYear();

    const mes = fecha.getMonth() + 1;
    
    const dia = fecha.getDate()+1;

    type PatID = {
    Documento : string
    }

    const list = await db.query("CALL patientIDs(:anio, :mes, :dia)", { replacements: { anio: anio, mes: mes, dia: dia } })as Array<PatID>;
    
    

    const ret = new Array<string>();

    list.forEach(element => {
        ret.push(element.Documento)
    });
    
    return ret;
}

//#endregion

//#region individual email schedulers

//recibe una lista de direcciones IDs de pacientes y les envia sus email en batches parametrizables
const scheduleEmailsPatients = (pacientes: (string)[]) => {
    const job = scheduleJob('0 0/1 * * *', async function () {
    //@ts-ignore
    const patientEmailsPerHour = await Config.findByPk("PATIENT_EMAILS_PER_HOUR");
    let counter = 0;
    if(patientEmailsPerHour){
    while (counter < parseInt(patientEmailsPerHour.Value)) {
      const destination = pacientes.shift();
      if (destination !== undefined) { sendEmailPat(destination) };
      counter++;
    }}
    })
    
}

//recibe una lista de direcciones IDs de profesionales y les envia sus email en batches parametrizables
const scheduleEmailsPersonnel = (ids: string[]) => {
    const job = scheduleJob('0 0/1 * * *', async function () {
      //@ts-ignore
      const profEmailsPerHour = await Config.findByPk("PROF_EMAILS_PER_HOUR");
      let counter = 0;
      if(profEmailsPerHour){
      while (counter < parseInt(profEmailsPerHour.Value)) {
        console.log(profEmailsPerHour)
        const id = ids.shift();
        if (id !== undefined) { sendEmailProf(id) };
        counter++;
      }}
    })
}

//#endregion

//#region senders

//construye y envia un email a un profesional
async function sendEmailProf(id: string) {
    const sender='"CETER" <noreply.dialysismanager@gmail.com>'
    const meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
    const mesActual = meses[new Date().getMonth()];
    const body = await buildBodyProf(id, mesActual);
    //@ts-ignore
    const professional = await Professional.findByPk(id);
    const subject = "CETER turnos mes de " + mesActual;
    if (professional !== null) {
        send(sender, professional.Correo.toString(), subject, body[0], body[1]);
    }
}

//construye y envia un email a un paciente
async function sendEmailPat(id: any) {
    const fecha = new Date();
    const mes = fecha.getMonth() + 1;
    const dia = fecha.getDate() + 1;
    //@ts-ignore
    const patient = await Patient.findByPk(id);
    const sender = '"CETER" <noreply.dialysismanager@gmail.com>';
    const subject = "Recordatorio de Dialisis " + dia + "/" + mes;
    const body = "Este es un recordatorio automatizado de su dialisis el dia de mañana " + dia + "/" + mes + ". Si necesita cambiar la fecha del tratamiento o su horario habitual, comuniquese con el centro de dialisis.";
    if (patient) {
        send(sender, patient.Correo, subject, body, body);
    }
    
}

//realiza la operacion de envio usando el smtpTransport
function send(sender:string, recipient:string, subject:string, plainText:string, html:string) {
     const outbound = smtpTransport.sendMail({
         from: sender, //'"CETER" <noreply.dialysismanager@gmail.com>',
         to: recipient,
         subject: subject,
         text: plainText,
         html: html
     });
}

//arma el cuerpo del email de profesional
async function buildBodyProf(id: string, mesActual: string): Promise<string[]> {
    const shifts: Shift[] = await queryShift.getAll();
    const shiftsProf: ShiftProf[] = await queryProf.currentMonthShifts(id); //TODO consigue los turnos y fechas de este profesional este mes
    let html: string = "<b>Sus turnos para el mes de " + mesActual + " son: </b> <br>";
    let plainText: string = "Sus turnos para el mes de " + mesActual + " son: ";
    
    shiftsProf.forEach(element => {
        if (element) {
            const index = element.CodTurno.toString();
            html= html.concat("-", element.Fecha.toString().substring(8), " de ", mesActual, ", turno ", shifts[parseInt(index)-1].Nombre, "<br>");
            plainText= plainText.concat(element.Fecha.toString().substring(8), " de ", mesActual, ", turno ",shifts[parseInt(index)-1].Nombre);
        }
    });

    const ret: string[] = [html, plainText];

    return ret;
}

//#endregion


