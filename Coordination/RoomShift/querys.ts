import RoomShift from "./roomshifts.model";


//@ts-ignore
export const getAll = async () => await RoomShift.findAll();

//@ts-ignore
export const findOne = async (body: any) => 
    //@ts-ignore
    await RoomShift.findOne({
        where: {
            CodSala: body.CodSala,
            CodTurno: body.CodTurno,
            Fecha: body.Fecha,
        },
    });

//@ts-ignore
export const getById = async (CodSala: number, CodTurno: number, Fecha: Date) =>
    //@ts-ignore
    await RoomShift.findOne({ where: { CodSala, CodTurno, Fecha} });