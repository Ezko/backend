//importamos sequelize y la conexion a la base de datos
import { DataTypes, Model } from "sequelize";
import db from "../../db/connection";

class RoomShift extends Model {
    public CodSala!: BigInt;
    public CodTurno!: BigInt;
    public Fecha!: Date;
    public CedulaMedico!: string;
    public CedulaLicenciado!: string;
}

//@ts-ignore
RoomShift.init(
    {
        CodSala: {
            type: DataTypes.BIGINT,
            primaryKey: true,
        },
        CodTurno: {
            type: DataTypes.BIGINT,
            primaryKey: true,
        },
        Fecha: {
            type: DataTypes.DATE,
            primaryKey: true,
        },
        CedulaMedico: {
            type: DataTypes.STRING,
        },
        CedulaLicenciado: {
            type: DataTypes.STRING,
        },
    },
    {
        //@ts-ignore
        tableName: "SalaTurno",
        sequelize: db,
    },
)

//exportamos como default
export default RoomShift;