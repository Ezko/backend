/**
 * Data Model Interfaces
 */
 import { Request, Response } from "express";
 import RoomShift from "./roomshifts.model";
 import * as query from "./querys";
 
 /**
  * Service Methods
  */
 
 export const getRoomShifts = async (req: Request, res: Response) => {
   //@ts-ignore
   const rs = await query.getAll();
 
   res.json({ rs });
 };
 
 export const getRoomShift = async (req: Request, res: Response) => {
    const { codSala, codTurno, fecha } = req.params;
 
   //@ts-ignore
   const rs = await query.getById(codSala, codTurno, fecha);
 
   if (rs) {
     res.json(rs);
   } else {
     res.status(200).json({
       msg: `No existe un turno con el id ${codSala} ${codTurno} ${fecha}`,
     });
   }
 };
 
 export const postRoomShift = async (req: Request, res: Response) => {
   const { body } = req;
 
   try {
     //@ts-ignore
     const existsRS = await query.getById(body.CodSala, body.CodTurno, body.Fecha);
 
     if (existsRS) {
       return res.status(400).json({
         msg: "Ya existe un turno con el código " + body.CodSala + " " + body.CodTurno + " " + body.Fecha,
       });
     }
 
     //@ts-ignore
     const rs = RoomShift.build({
       CodSala: body.CodSala,
       CodTurno: body.CodTurno,
       Fecha: body.Fecha,
       CedulaMedico: body.CedulaMedico,
       CedulaLicenciado: body.CedulaLicenciado,
     });
     await rs.save();
 
     res.json(rs);
   } catch (error) {
     console.log(error);
     res.status(500).json({
       msg: "Hable con el administrador",
     });
   }
 };
 
 export const putRoomShift = async (req: Request, res: Response) => {
   const { id } = req.params;
   const { body } = req;
 
   try {
     //@ts-ignore
     var rs = await query.getById(body.CodSala, body.CodTurno, body.Fecha);
     if (!rs) {
       //@ts-ignore
       rs = RoomShift.build({
            CodSala: body.CodSala,
            CodTurno: body.CodTurno,
            Fecha: body.Fecha,
            CedulaMedico: body.CedulaMedico,
            CedulaLicenciado: body.CedulaLicenciado,
       });
       await rs.save();
     } else {
       await rs.update(body);
     }
 
     res.json(rs);
   } catch (error) {
     console.log(error);
     res.status(500).json({
       msg: "Hable con el administrador",
     });
   }
 };
 
 export const deleteRoomShift = async (req: Request, res: Response) => {
    const { codSala, codTurno, fecha } = req.params;
   //@ts-ignoreclear
   const rs = await query.getById(codSala, codTurno, fecha);
   if (!rs) {
     return res.status(200).json({
       msg: "No existe un turno con el id " + codSala + " " + codTurno + " " + fecha,
     });
   }
 
   await rs.update({ estado: false });
 
   await rs.destroy();
 
   res.json(rs);
 };
 