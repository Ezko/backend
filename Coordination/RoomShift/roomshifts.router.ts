/**
 * Required External Modules and Interfaces
 */

 import { Router } from "express";
import { requireJwtMiddleware } from "../../middleware/auth.middleware";
 import { getRoomShifts, getRoomShift, postRoomShift, putRoomShift, deleteRoomShift } from "../RoomShift/roomshifts.service";
 
 /**
  * Router Definition
  */
 
const rsRouter = Router();
 
rsRouter.use(requireJwtMiddleware);

 
 // routing of functions getAll, getOne, post, put, and delete
 rsRouter.get("/", getRoomShifts);
 rsRouter.get("/:codSala/:codTurno/:fecha", getRoomShift);
 rsRouter.post("/", postRoomShift);
 rsRouter.put("/:codSala/:codTurno/:fecha", putRoomShift);
 rsRouter.delete("/:codSala/:codTurno/:fecha", deleteRoomShift);
 
 export default rsRouter;