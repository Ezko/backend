import Shift from "./shift.model";

//@ts-ignore
export const findAll = async () => await Shift.findAll();

//@ts-ignore
export const findOne = async (body: any) => 
    //@ts-ignore
    await Shift.findOne({
        where: {
            CodTurno: body.CodTurno,
        },
    });

//@ts-ignore
export const findById = async(id: number) => await Shift.findByPk(id);