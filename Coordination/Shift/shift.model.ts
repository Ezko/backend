//importamos sequelize y la conexion a la base de datos
import { DataTypes, Model } from "sequelize";
import db from "../../db/connection";

class Shift extends Model {
  public CodTurno!: string;
  public HComienzo!: Date;
  public HFin!: Date;
  public Nombre!: string;
}

//@ts-ignore
Shift.init(
  {
      CodTurno: {
          type: DataTypes.STRING,
          primaryKey: true
      },
      HComienzo: {
          type: DataTypes.STRING,
      },
      HFin: {
          type: DataTypes.STRING,
      },
      Nombre: {
          type: DataTypes.STRING,
      },
  }, 
  {
      //@ts-ignore
      tableName: "Turno",
      sequelize: db,
  }
)

//exportamos como default
export default Shift;