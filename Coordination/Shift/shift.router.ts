import { Router } from "express";
import { requireJwtMiddleware } from "../../middleware/auth.middleware";
import { getShifts, getShift, postShift, putShift, deleteShift } from "./shift.service";

/**
 * Router Definition
 */

const shiftsRouter = Router();

shiftsRouter.use(requireJwtMiddleware);


shiftsRouter.get("/", getShifts);
shiftsRouter.get("/:id", getShift);
shiftsRouter.post("/", postShift);
shiftsRouter.put("/:id", putShift);
shiftsRouter.delete("/:id", deleteShift);

export default shiftsRouter;