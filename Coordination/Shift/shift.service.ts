/**
 * Data Model Interfaces
 */
 import { Request, Response } from "express";
 import Shift from "./shift.model";
 import * as query from "./querys";

 /**
 * Service Methods
 */

 export const getShifts = async (req: Request, res: Response) => {
     
    //@ts-ignore
     const shifts = await query.findAll();

     res.json({ shifts });
 };

 export const getShift = async (req: Request, res: Response) => {
     const { id } = req.params;

     //@ts-ignore
    const shift = await query.findById(id);

    if (shift) {
        res.json(shift);
    } else {
        res.status(200).json({
            msg: `No existe un turno con el código ${id}`
        });
    }
 };

 export const postShift = async (req: Request, res: Response) => {
    const { body } = req;

    try {
        //@ts-ignore
        const existsCodigo = await query.findOne(body);

        if (existsCodigo) {
            return res.status(400).json({
                msg: "Ya existe un turno con el código " + body.CodTurno,
            });
        }

        //@ts-ignore
        const shift = Shift.build({
            CodTurno: body.CodTurno,
            HComienzo: body.HComienzo,
            HFin: body.HFin,
            Nombre: body.Nombre,
        });

        await shift.save();

        res.json(shift);

    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Hable con el administrador",
        });
    }
};

export const putShift = async (req: Request, res: Response) => {
    const { id } = req.params;
    const { body } = req;

    try {
        //@ts-ignore
        var shift = await query.findById(id);
        if (!shift) {
            //@ts-ignore
            shift = Shift.build({
                CodTurno: body.CodTurno,
                HComienzo: body.HComienzo,
                HFin: body.HFin,
                Nombre: body.Nombre,
            });

            await shift.save();

        } else {
            
            await shift.update(body);

        }

        res.json(shift);

    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Hable con el administrador",
        });
    }
};

export const deleteShift = async (req: Request, res: Response) => {
    const { id } = req.params;

    //@ts-ignoreclear
    const shift = await query.findById(id);

    if (!shift) {
        return res.status(200).json({
            msg: "No existe un turno con el código " + id,
        });
    }

    await shift.update({ estado: false });

    await shift.destroy();

    res.json(shift);
};