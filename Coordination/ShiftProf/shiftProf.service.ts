/**
 * Data Model Interfaces
 */
 import bodyParser from "body-parser";
import { Request, Response } from "express";
 import ShiftProf from "./shiftProf.model";
import * as query from "./querys"
 
 /**
 * Service Methods
 */

 export const getShiftProfs = async (req: Request, res: Response) => {
     
    //@ts-ignore
     const shiftProfs = await query.findAll();

     res.json({ shiftProfs });
 };

 export const getShiftProf = async (req: Request, res: Response) => {
     const { cedula, fecha } = req.params;

     //@ts-ignore
    const shiftProf = await query.findOne(cedula, fecha);

    if (shiftProf) {
        res.json(shiftProf);
    } else {
        res.status(200).json({
            msg: `No existe un turno del profesional con la cedula ${cedula} en la fecha ${fecha}`
        });
    }
 };

 export const postShiftProf = async (req: Request, res: Response) => {
    const { body } = req;

    try {
        //@ts-ignore
        const existsCedula = await query.findOne(body.Cedula, body.Fecha);

        if (existsCedula) {
            return res.status(400).json({
                msg: "Ya existe un profesional con la cédula " + body.Cedula,
            });
        }

        //@ts-ignore
        const shiftProf = ShiftProf.build({
            Cedula: body.Cedula,
            Fecha: body.Fecha,
            CodTurno: body.CodTurno,
        });

        await shiftProf.save();

        res.json(shiftProf);

    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Hable con el administrador",
        });
    }
};

export const putShiftProf = async (req: Request, res: Response) => {
    const { body } = req;

    try {
        //@ts-ignore
        var shiftProf = await query.findOne(body.Cedula, body.Fecha);
        if (!shiftProf) {
            //@ts-ignore
            shiftProf = ShiftProf.build({
                Cedula: body.Cedula,
                Fecha: body.Fecha,
                CodTurno: body.CodTurno,
            });

            await shiftProf.save();

        } else {
            
            await shiftProf.update(body);

        }

        res.json(shiftProf);

    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Hable con el administrador",
        });
    }
};

export const deleteShiftProf = async (req: Request, res: Response) => {
    const { body } = req;

    //@ts-ignoreclear
    const shiftProf = await query.findOne(body.Cedula, body.Fecha);

    if (!shiftProf) {
        return res.status(200).json({
            msg: "No existe un profesional con la cédula " + body.Cedula,
        });
    }

    await shiftProf.update({ estado: false });

    await shiftProf.destroy();

    res.json(shiftProf);
};