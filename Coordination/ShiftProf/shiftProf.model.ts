//importamos sequelize y la conexion a la base de datos
import { DataTypes, Model } from "sequelize";
import db from "../../db/connection";
import Sala from "../../Infrastructure/Rooms/room.model";
import Turno from "../../Coordination/Shift/shift.model"

class ShiftProf extends Model {
    public Cedula!: string;
    public Fecha!: Date;
    public CodTurno!: bigint;
    public CodSala!: bigint;
}

//@ts-ignore
ShiftProf.init(
    {
        Cedula: {
            type: DataTypes.STRING,
            primaryKey: true,
        },
        Fecha: {
            type: DataTypes.DATEONLY,
            primaryKey: true,
        },
        CodTurno: {
            type: DataTypes.BIGINT,
            references: {
                model: Turno,
                key: "CodTurno",
            },
        },
    }, 
    {
        //@ts-ignore
        tableName: "TurnoProf",
        sequelize: db,
    }
);

//exportamos como default
export default ShiftProf;