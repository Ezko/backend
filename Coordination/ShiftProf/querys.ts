import ShiftProf from "./shiftProf.model";
import {Op} from "sequelize";

//@ts-ignore
export const findAll = async () => await ShiftProf.findAll();

//@ts-ignore
export const findOne = async (cedula: string, fecha:string) => 
    //@ts-ignore
    await ShiftProf.findOne({
        where: {
            Cedula: cedula,
            Fecha: fecha,
        },
    });

//@ts-ignore
export const findById = async(id: number) => await ShiftProf.findByPk(id);

export const currentMonthShifts = async (id: string) => {
    const date = new Date();
    const currDate = date.setMonth(date.getMonth());
    const targetDate = date.setMonth(date.getMonth()+1);
    //@ts-ignore
    return ShiftProf.findAll({
        where: {
            Cedula : id,
            Fecha : {[Op.between] : [currDate, targetDate]}
        }
    })
}

