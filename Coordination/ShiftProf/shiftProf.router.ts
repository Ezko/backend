import { Router } from "express";
import { requireJwtMiddleware } from "../../middleware/auth.middleware";
import { getShiftProfs, getShiftProf, postShiftProf, putShiftProf, deleteShiftProf } from "./shiftProf.service";

/**
 * Router Definition
 */

const shiftProfRouter = Router();

shiftProfRouter.use(requireJwtMiddleware);


shiftProfRouter.get("/", getShiftProfs);
shiftProfRouter.get("/:cedula/:fecha", getShiftProf);
shiftProfRouter.post("/", postShiftProf);
shiftProfRouter.put("/:cedula/:fecha", putShiftProf);
shiftProfRouter.delete("/:cedula/:fecha", deleteShiftProf);

export default shiftProfRouter;