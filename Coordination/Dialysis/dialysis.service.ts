/**
 * Data Model Interfaces
 */
import { Request, Response } from "express";
import Dialysis from "./dialysis.model";
import * as query from "./querys";

/**
 * Service Methods
 */

export const getDialysis = async (req: Request, res: Response) => {
  //@ts-ignore
  const dialysis = await query.getAll();

  res.json({ dialysis });
};

export const getDialysisOne = async (req: Request, res: Response) => {
  const { id } = req.params;

  //@ts-ignore
  const dialysisOne = await query.findById(id);

  if (dialysisOne) {
    res.json(dialysisOne);
  } else {
    res.status(200).json({
      msg: `No existe una sesión con el id ${id}`,
    });
  }
};

export const getDialysisShiftRoom = async (req: Request, res: Response) => {
  const { codSala, codTurno, fecha } = req.params;

  // const iniDate = new Date(parseInt(fecha))
  const parts = fecha.split("-");
  const iniDate = new Date(
    Date.UTC(parseInt(parts[0]), parseInt(parts[1]) - 1, parseInt(parts[2]))
  );

  //@ts-ignore
  res.json(await query.dilaysisShiftRoomDate(+codSala, +codTurno, iniDate));
};

export const getAvailability = async (req: Request, res: Response) => {
  const { fechaIni, fechaFin } = req.params;
  let partsIni = fechaIni.split("-");
  let iniDate = new Date(
    Date.UTC(
      parseInt(partsIni[0]),
      parseInt(partsIni[1]) - 1,
      parseInt(partsIni[2])
    )
  );
  let partsFin = fechaFin.split("-");
  let finDate = new Date(
    Date.UTC(
      parseInt(partsFin[0]),
      parseInt(partsFin[1]) - 1,
      parseInt(partsFin[2])
    )
  );
  //@ts-ignore
  res.json(await query.getShiftAvailability(iniDate, finDate, true));
};

export const getDialysisPatientBetween = async (
  req: Request,
  res: Response
) => {
  const { codPaciente, fechaIni, fechaFin } = req.params;
  let partsIni = fechaIni.split("-");
  let iniDate = new Date(
    Date.UTC(
      parseInt(partsIni[0]),
      parseInt(partsIni[1]) - 1,
      parseInt(partsIni[2])
    )
  );
  let partsFin = fechaFin.split("-");
  let finDate = new Date(
    Date.UTC(
      parseInt(partsFin[0]),
      parseInt(partsFin[1]) - 1,
      parseInt(partsFin[2])
    )
  );
  res.json(
    //@ts-ignore
    await query.patientDialysisBetweenDates(codPaciente, iniDate, finDate)
  );
};

export const postDialysis = async (req: Request, res: Response) => {
  const { project } = req.params;
  const { body } = req;

  try {
    //@ts-ignore
    const existsID = await query.findOne(body);

    if (existsID) {
      return res.status(400).json({
        msg: "Ya existe una sesion con la misma maquina, turno y dia",
      });
    }

    //@ts-ignore
    const dialysis = Dialysis.build({
      Fecha: body.Fecha,
      EventoPirogenico: body.EventoPirogenico,
      CodTurno: body.CodTurno,
      CedulaEnfermero: body.CedulaEnfermero,
      DocPaciente: body.DocPaciente,
      CodMaquina: body.CodMaquina,
      CedulaMedico: body.CedulaMedico,
      CedulaLicenciado: body.CedulaLicenciado,
    });

    await dialysis.save();

    if (project === "true") {
      const dateProjection = new Date(body.Fecha);
      dateProjection.setDate(dateProjection.getDate() + 7);
      const otherDate = new Date(dateProjection);
      otherDate.setDate(otherDate.getDate() + 28);
      while (!(await query.isEmpty(dateProjection, otherDate))) {
        //@ts-ignore
        const dialysisProjection = Dialysis.build({
          Fecha: dateProjection.toISOString().substring(0, 10),
          EventoPirogenico: body.EventoPirogenico,
          CodTurno: body.CodTurno,
          CedulaEnfermero: body.CedulaEnfermero,
          DocPaciente: body.DocPaciente,
          CodMaquina: body.CodMaquina,
          CedulaMedico: body.CedulaMedico,
          CedulaLicenciado: body.CedulaLicenciado,
        });
        await dialysisProjection.save();
        dateProjection.setDate(dateProjection.getDate() + 7);
      }
    }
    res.json(dialysis);
  } catch (error) {
    console.log(error);
    res.status(500).json({
      msg: "Hable con el administrador",
    });
  }
};

export const putDialysis = async (req: Request, res: Response) => {
  const { id, project } = req.params;
  const { body } = req;

  try {
    //@ts-ignore
    var dialysis = await query.findById(id);
    if (!dialysis) {
      //@ts-ignore
      dialysis = Dialysis.build({
        Fecha: body.Fecha,
        EventoPirogenico: body.EventoPirogenico,
        CodTurno: body.CodTurno,
        CedulaEnfermero: body.CedulaEnfermero,
        DocPaciente: body.DocPaciente,
        CodMaquina: body.CodMaquina,
        CedulaMedico: body.CedulaMedico,
        CedulaLicenciado: body.CedulaLicenciado,
      });

      await dialysis.save();

      if (project === "true") {
        const dateProjection = new Date(body.Fecha);
        dateProjection.setDate(dateProjection.getDate() + 7);
        const otherDate = new Date(dateProjection);
        otherDate.setDate(otherDate.getDate() + 28);
        while (!(await query.isEmpty(dateProjection, otherDate))) {
          //@ts-ignore
          const dialysisProjection = Dialysis.build({
            Fecha: dateProjection.toISOString().substring(0, 10),
            EventoPirogenico: body.EventoPirogenico,
            CodTurno: body.CodTurno,
            CedulaEnfermero: body.CedulaEnfermero,
            DocPaciente: body.DocPaciente,
            CodMaquina: body.CodMaquina,
            CedulaMedico: body.CedulaMedico,
            CedulaLicenciado: body.CedulaLicenciado,
          });
          body.Fecha = dateProjection.toISOString().substring(0, 10);
          await dialysisProjection.save();
          dateProjection.setDate(dateProjection.getDate() + 7);
        }
      }
    } else {
      let searchCriteria = {
        Fecha: body.Fecha,
        CodTurno: dialysis.CodTurno,
        CodMaquina: dialysis.CodMaquina,
      };
      await dialysis.update(body);
      if (project === "true") {
        const dateProjection = new Date(body.Fecha);
        dateProjection.setDate(dateProjection.getDate() + 7);
        const otherDate = new Date(dateProjection);
        otherDate.setDate(otherDate.getDate() + 28);
        while (!(await query.isEmpty(dateProjection, otherDate))) {
          searchCriteria.Fecha = dateProjection.toISOString().substring(0, 10);
          //@ts-ignore
          const dialysisProjection = Dialysis.build({
            Fecha: dateProjection.toISOString().substring(0, 10),
            EventoPirogenico: body.EventoPirogenico,
            CodTurno: body.CodTurno,
            CedulaEnfermero: body.CedulaEnfermero,
            DocPaciente: body.DocPaciente,
            CodMaquina: body.CodMaquina,
            CedulaMedico: body.CedulaMedico,
            CedulaLicenciado: body.CedulaLicenciado,
          });
          body.Fecha = dateProjection.toISOString().substring(0, 10);
          const target = await query.findOne(searchCriteria);
          if (target) {
            await target.update(body);
          }
          dateProjection.setDate(dateProjection.getDate() + 7);
        }
      }
    }

    res.json(dialysis);
  } catch (error) {
    console.log(error);
    res.status(500).json({
      msg: "Hable con el administrador",
    });
  }
};

export const deleteDialysis = async (req: Request, res: Response) => {
  const { id, project } = req.params;

  //@ts-ignoreclear
  const dialysis = await query.findById(id);

  if (!dialysis) {
    return res.status(200).json({
      msg: "No existe una sesión con el código " + id,
    });
  }
  const contFecha = new Date(dialysis.Fecha).toISOString().substring(0, 10);
  let searchCriteria = {
    Fecha: contFecha,
    CodTurno: dialysis.CodTurno,
    CodMaquina: dialysis.CodMaquina,
  };
  await dialysis.update({ estado: false });
  await dialysis.destroy();
  if (project === "true") {
    const dateProjection = new Date(dialysis.Fecha);
    dateProjection.setDate(dateProjection.getDate() + 7);
    const otherDate = new Date(dateProjection);
    otherDate.setDate(otherDate.getDate() + 28);
    while (!(await query.isEmpty(dateProjection, otherDate))) {
      searchCriteria.Fecha = dateProjection.toISOString().substring(0, 10);
      const target = await query.findOne(searchCriteria);
      if (target) {
        console.log("destroyed", target);
        await target.update({ estado: false });
        await target.destroy();
      }
      dateProjection.setDate(dateProjection.getDate() + 7);
    }
  }

  res.json(dialysis);
};
