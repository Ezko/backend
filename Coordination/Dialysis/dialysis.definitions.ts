

export type inboundDialysis = {
    DialisisID: number,
    Fecha: string,
    EventoPirogenico: string,
    CodTurno: number,
    CedulaEnfermero: string,
    DocPaciente: string,
    CodMaquina: number
    CedulaMedico: string,
    CedulaLicenciado: string,
    NombreEnfermero: string,
    ApellidoEnfermero: string,
    NombrePaciente: string,
    ApellidoPaciente: string,
    CodPico: number,
    CodSala: number
}

export type doctorNom = {
    NombreMedico: string;
    ApellidoMedico: string;
  }

export type licenseeNom = {
    NombreLicenciado: string;
    ApellidoLicenciado: string;
}

export class LicenseeName {
    licenseeFirstName: string;
    licenseeLastName: string;

    constructor(firstName: string, lastName: string) {
        this.licenseeFirstName = firstName;
        this.licenseeLastName = lastName;
}
}

export class DoctorName {
    doctorFirstName: string;
    doctorLastName: string;

    constructor(firstName: string, lastName: string) {
        this.doctorFirstName = firstName;
        this.doctorLastName = lastName;
    }
}

export class DilaysisShiftRoomDateClass {
    dialysisId: number;
    date: Date;
    pyrogenicEvent: string;
    shiftId: number;
    nurseId: string;
    patientId: string;
    machineId: number;
    doctorId: string;
    licenseeId: string;
    nurseFirstName: string;
    nurseLastName: string;
    patientFirstName: string;
    patientLastName: string;
    valveId: number;
    roomId: number;

    constructor(dialysisId: number, date: string, pyrogenicEvent: string, shiftId: number, nurseId: string, patientId: string, machineId: number, doctorId: string, licenseeId: string, nurseFirstName: string, nurseLastName: string, patientFirstName: string, patientLastName: string, valveId: number, roomId: number) {
        this.dialysisId = dialysisId;
        const parts = date.split('-');
        this.date = new Date(parseInt(parts[0]), parseInt(parts[1]) - 1, parseInt(parts[2])); 
        this.pyrogenicEvent = pyrogenicEvent;
        this.shiftId = shiftId;
        this.nurseId = nurseId;
        this.patientId = patientId;
        this.machineId = machineId;
        this.doctorId = doctorId;
        this.licenseeId=licenseeId
        this.nurseFirstName = nurseFirstName;
        this.nurseLastName = nurseLastName;
        this.patientFirstName = patientFirstName;
        this.patientLastName = patientLastName;
        this.valveId = valveId;
        this.roomId = roomId;
    }
}

export type shiftType = {
    CodTurno : number,
    Nombre : string,
    Fecha: Date;
}

export type shiftRoom = {
    CodSala: number,
    CodTurno: number,
    Fecha: number,
    CedulaMedico: string,
    CedulaLicenciado: string
}