import { Router } from "express";
import { requireJwtMiddleware } from "../../middleware/auth.middleware";
import {
  getDialysis,
  getDialysisOne,
  postDialysis,
  putDialysis,
  deleteDialysis,
  getDialysisShiftRoom,
  getAvailability,
  getDialysisPatientBetween,
} from "./dialysis.service";

/**
 * Router Definition
 */

const dialysisRouter = Router();

dialysisRouter.use(requireJwtMiddleware);

dialysisRouter.get("/", getDialysis);
dialysisRouter.get("/:id", getDialysisOne);
dialysisRouter.get(
  "/populateShift/:codSala/:codTurno/:fecha",
  getDialysisShiftRoom
);
dialysisRouter.get("/availability/:fechaIni/:fechaFin", getAvailability);
dialysisRouter.get(
  "/patientScheduledDialysis/:codPaciente/:fechaIni/:fechaFin",
  getDialysisPatientBetween
);
dialysisRouter.post("/:project", postDialysis);
dialysisRouter.put("/:id/:project", putDialysis);
dialysisRouter.delete("/:id/:project", deleteDialysis);

export default dialysisRouter;
