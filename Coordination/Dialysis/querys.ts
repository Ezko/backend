import db from "../../db/connection";
import Room from "../../Infrastructure/Rooms/room.model";
import Shift from "../Shift/shift.model";
import ShiftProf from "../ShiftProf/shiftProf.model";
import {
  DilaysisShiftRoomDateClass,
  DoctorName,
  doctorNom,
  inboundDialysis,
  LicenseeName,
  licenseeNom,
  shiftType,
} from "./dialysis.definitions";
import Dialysis from "./dialysis.model";

//@ts-ignore
export const getAll = async () => await Dialysis.findAll();

//@ts-ignore
export const findOne = async (body: any) =>
  //@ts-ignore

  await Dialysis.findOne({
    where: {
      Fecha: body.Fecha,
      CodTurno: body.CodTurno,
      CodMaquina: body.CodMaquina,
    },
  });

//@ts-ignore
export const findById = async (id: number) => await Dialysis.findByPk(id);

//@ts-ignore
export const dilaysisShiftRoomDate = async (
  codSala: number,
  codTurno: number,
  fecha: Date
) => {
  let resultsMedico = (await db.query(
    "CALL MedicoSalaTurno(:fecha, :codSala, :codTurno)",
    {
      replacements: {
        fecha: fecha.toISOString().substring(0, 10),
        codSala: codSala,
        codTurno: codTurno,
      },
    }
  )) as Array<doctorNom>;

  let resultsLicensiado = (await db.query(
    "CALL LicenciadoSalaTurno(:fecha, :codSala, :codTurno)",
    {
      replacements: {
        fecha: fecha.toISOString().substring(0, 10),
        codSala: codSala,
        codTurno: codTurno,
      },
    }
  )) as Array<licenseeNom>;

  const resultsDialysis = (await db.query(
    "CALL DialisisSalaTurno(:fecha, :codSala, :codTurno)",
    {
      replacements: {
        fecha: fecha.toISOString().substring(0, 10),
        codSala: codSala,
        codTurno: codTurno,
      },
    }
  )) as Array<inboundDialysis>;
  let retDialisis: DilaysisShiftRoomDateClass[] =
    new Array<DilaysisShiftRoomDateClass>();

  let retLicensiado = null;
  let retMedico = null;
  if (resultsLicensiado.length > 0) {
    retLicensiado = new LicenseeName(
      resultsLicensiado[0].NombreLicenciado,
      resultsLicensiado[0].ApellidoLicenciado
    );
  }
  if (resultsMedico.length > 0) {
    retMedico = new DoctorName(
      resultsMedico[0].NombreMedico,
      resultsMedico[0].ApellidoMedico
    );
  }

  resultsDialysis.forEach((element) => {
    const transform = new DilaysisShiftRoomDateClass(
      element.DialisisID,
      element.Fecha,
      element.EventoPirogenico,
      element.CodTurno,
      element.CedulaEnfermero,
      element.DocPaciente,
      element.CodMaquina,
      element.CedulaMedico,
      element.CedulaLicenciado,
      element.NombreEnfermero,
      element.ApellidoEnfermero,
      element.NombrePaciente,
      element.ApellidoPaciente,
      element.CodPico,
      element.CodSala
    );
    retDialisis.push(transform);
  });
  return [retDialisis, retMedico, retLicensiado];
};

//@ts-ignore
export const getShiftAvailability = async (fechaIni: Date, fechaFin: Date, inherit :boolean) => {
  const nextMonday = new Date();
  nextMonday.setHours(0, 0, 0, 0);
  nextMonday.setDate(nextMonday.getDate() + 1);
  while (nextMonday.getDay() !== 1) {
    nextMonday.setDate(nextMonday.getDate() + 1);
  }
  if (fechaIni.valueOf() >= nextMonday.valueOf() && inherit) {
    await importLastWeek(new Date(fechaIni));
  }
  //@ts-ignore
  const shifts = (await Shift.findAll()) as Array<shiftType>;

  type disponibilidadType = {
    CodTurno: number;
    Nombre: string;
    Fecha: string;
    PlazasOcupadas: number;
    CapacidadTotalTurno: number;
  };

  class DisponibilidadClass {
    shiftId: number;
    name: string;
    date: number;
    state: string;

    constructor(
      shiftId: number,
      name: string,
      date: string,
      capacity: number,
      occupancy: number
    ) {
      this.shiftId = shiftId;
      this.name = name;
      this.date = new Date(date).getTime();
      if (occupancy > 0 && occupancy < capacity) {
        this.state = "PARTIAL";
      } else if (occupancy === 0) {
        this.state = "EMPTY";
      } else {
        this.state = "FULL";
      }
    }
  }

  let ret: DisponibilidadClass[] = new Array<DisponibilidadClass>();
  let counterDate: Date = new Date(fechaIni);
  while (counterDate <= fechaFin) {
    shifts.forEach((element) => {
      ret.push(
        new DisponibilidadClass(
          element.CodTurno,
          element.Nombre,
          counterDate.toISOString().substring(0, 10),
          0,
          0
        )
      );
    });
    counterDate.setDate(counterDate.getDate() + 1);
  }

  //@ts-ignore
  const results = (await db.query("CALL DevolverTurnos(:fecha1, :fecha2)", {
    replacements: {
      fecha1: fechaIni.toISOString().substring(0, 10),
      fecha2: fechaFin.toISOString().substring(0, 10),
    },
  })) as Array<disponibilidadType>;

  results.forEach((element) => {
    const transform = new DisponibilidadClass(
      element.CodTurno,
      element.Nombre,
      element.Fecha,
      element.CapacidadTotalTurno,
      element.PlazasOcupadas
    );
    ret.forEach((e) => {
      if (e.date === transform.date && e.shiftId === transform.shiftId) {
        ret[ret.indexOf(e)] = transform;
      }
    });
  });

  return ret;
};

export const patientDialysisBetweenDates = async (
  docPaciente: string,
  fechaIni: Date,
  fechaFin: Date
) => {
  const ret: DilaysisShiftRoomDateClass[] =
    new Array<DilaysisShiftRoomDateClass>();

  //@ts-ignore
  const results = (await db.query(
    "Call PacienteDialisisFechas(:docPaciente, :fechaIni, :fechaFin)",
    {
      replacements: {
        docPaciente: docPaciente,
        fechaIni: fechaIni.toISOString().substring(0, 10),
        fechaFin: fechaFin.toISOString().substring(0, 10),
      },
    }
  )) as Array<inboundDialysis>;

  results.forEach((element) => {
    const transform = new DilaysisShiftRoomDateClass(
      element.DialisisID,
      element.Fecha,
      element.EventoPirogenico,
      element.CodTurno,
      element.CedulaEnfermero,
      element.DocPaciente,
      element.CodMaquina,
      element.CedulaMedico,
      element.CedulaLicenciado,
      element.NombreEnfermero,
      element.ApellidoEnfermero,
      element.NombrePaciente,
      element.ApellidoPaciente,
      element.CodPico,
      element.CodSala
    );
    ret.push(transform);
  });

  return ret;
};

async function importLastWeek(futureWeek: Date) {
  //guarda la ultima semana pasada en una semana futura
  const fechaMadre: Date = new Date(futureWeek);
  let rewindCap = 0;
  while (fechaMadre >= new Date()) {
    fechaMadre.setDate(fechaMadre.getDate() - 7);
    rewindCap++;
  }
  if (rewindCap > 5) {
    return
  }

  let counter = 0;
  while (counter < 7) {
    //@ts-ignore
    const results = (await db.query("CALL DialisisDia(:fecha)", {
      replacements: { fecha: fechaMadre.toISOString().substring(0, 10) },
    })) as Array<inboundDialysis>;
    results.forEach(async (element) => {
      const transform = new DilaysisShiftRoomDateClass(
        element.DialisisID,
        futureWeek.toISOString().substring(0, 10),
        element.EventoPirogenico,
        element.CodTurno,
        element.CedulaEnfermero,
        element.DocPaciente,
        element.CodMaquina,
        element.CedulaMedico,
        element.CedulaLicenciado,
        element.NombreEnfermero,
        element.ApellidoEnfermero,
        element.NombrePaciente,
        element.ApellidoPaciente,
        element.CodPico,
        element.CodSala
      );
      if (!(await bajaTransitorios(transform, fechaMadre, futureWeek))) {
        //@ts-ignore
        const existsDialisis = await Dialysis.findOne({
          where: {
            Fecha: transform.date,
            CodTurno: transform.shiftId,
            CodMaquina: transform.machineId,
          },
        });
        //@ts-ignore
        const existsTurnoMedico = await ShiftProf.findOne({
          where: {
            Cedula: transform.doctorId,
            Fecha: transform.date,
            CodTurno: transform.shiftId,
          },
        });
        //@ts-ignore
        const existsTurnoLicensiado = await ShiftProf.findOne({
          where: {
            Cedula: transform.licenseeId,
            Fecha: transform.date,
            CodTurno: transform.shiftId,
          },
        });
        if (!existsDialisis) {
          //@ts-ignore
          const dialysis = await Dialysis.create({
            Fecha: transform.date,
            EventoPirogenico: transform.pyrogenicEvent,
            CodTurno: transform.shiftId,
            CedulaEnfermero: transform.nurseId,
            DocPaciente: transform.patientId,
            CodMaquina: transform.machineId,
            CedulaMedico: transform.doctorId,
            CedulaLicenciado: transform.licenseeId,
          });
          if (!existsTurnoMedico) {
            //@ts-ignore
            const shiftDoctor = await ShiftProf.create({
              Cedula: transform.doctorId,
              Fecha: transform.date,
              CodTurno: transform.shiftId,
            });
          }
          if (!existsTurnoLicensiado) {
            //@ts-ignore
            const shiftLicensee = await ShiftProf.create({
              Cedula: transform.licenseeId,
              Fecha: transform.date,
              CodTurno: transform.shiftId,
            });
          }
        }
      }
    });
    fechaMadre.setDate(fechaMadre.getDate() + 1);
    futureWeek.setDate(futureWeek.getDate() + 1);
    counter++;
  }
}

async function bajaTransitorios(
  base: DilaysisShiftRoomDateClass,
  fechaIni: Date,
  fechaFin: Date
) {
  type IdPat = {
    Documento: string;
  };
  const caducados = (await db.query(
    "CALL TransitoriosCaducados(:fecha1, :fecha2)",
    {
      replacements: {
        fecha1: fechaIni.toISOString().substring(0, 10),
        fecha2: fechaFin.toISOString().substring(0, 10),
      },
    }
  )) as Array<IdPat>;
  const strCaducados = new Array<string>();
  caducados.forEach((element) => {
    strCaducados.push(element.Documento);
  }); //TODO que chequee que el transitorio no se haya vuelto cronico

  if (strCaducados.indexOf(base.patientId) != -1) {
    return true;
  } else {
    return false;
  }
}


export const isEmpty = async (fechaIni: Date, fechaFin: Date) => {
  const timeframe = await getShiftAvailability(fechaIni, fechaFin, false);
  let flag = true;
  timeframe.forEach(disponibilidad => {
    if (disponibilidad.state != "EMPTY") {
      flag = false;
    }
  });
  return flag;
}