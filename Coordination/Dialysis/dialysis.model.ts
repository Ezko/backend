//importamos sequelize y la conexion a la base de datos
import { DataTypes, Model } from "sequelize";
import db from "../../db/connection";
import Medico from "../../Persons/Doctors/doctor.model";
import Turno from "../Shift/shift.model";
import Enfermero from "../../Persons/Nurses/nurse.model";
import Paciente from "../../Persons/Patients/patient.model";
import Maquina from "../../Infrastructure/Machines/machine.model";
import Licensee from "../../Persons/Licensees/licensee.model";

class Dialysis extends Model {
    public DialisisID!: BigInt;
    public Fecha!: Date;
    public EventoPirogenico!: string;
    public CodTurno!: BigInt;
    public CedulaEnfermero!: string;
    public DocPaciente!: string;
    public CodMaquina!: BigInt;
    public CedulaMedico!: string;
    public CedulaLicenciado!: string;
}

//@ts-ignore
Dialysis.init(
    {
        DialisisID: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true,
        },
        Fecha: {
            type: DataTypes.DATEONLY,
        },
        EventoPirogenico: {
            type: DataTypes.STRING,
        },
        CodTurno: {
            type: DataTypes.BIGINT,
            references: {
                model: Turno,
                key: "CodTurno",
            },
        },
        CedulaEnfermero: {
            type: DataTypes.STRING,
            references: {
                model: Enfermero,
                key: "Cedula",
            },
        },
        DocPaciente: {
            type: DataTypes.STRING,
            references: {
                model: Paciente,
                key: "Documento",
            },
        },
        CodMaquina: {
            type: DataTypes.BIGINT,
            references: {
                model: Maquina,
                key: "CodMaquina",
            },
        },
        CedulaMedico: {
            type: DataTypes.STRING,
            references: {
                model: Medico,
                key: "Cedula",
            },
        },
        CedulaLicenciado: {
            type: DataTypes.STRING,
            references: {
                model: Licensee,
                key: "Cedula",
            },
        }
    },
    {
        //@ts-ignore
        tableName: "Dialisis",
        sequelize: db,
    },
)

//exportamos como default
export default Dialysis;