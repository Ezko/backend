// Manejador de eror 404

import { Request, Response, NextFunction } from "express";

export const notFoundHandler = (
  request: Request,
  response: Response,
  next: NextFunction
) => {

  const message = "Pagina no encontrada";

  response.status(404).send(message);
};