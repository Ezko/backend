import { Request, Response, NextFunction } from "express";
import { DecodeResult, Session } from "./auth.definitions";
import {
  checkExpirationStatus,
  decodeSession,
  ExpirationStatus,
} from "./Jwt.middleware";

export function requireJwtMiddleware(
  request: Request,
  response: Response,
  next: NextFunction
) {
  const unauthorized = (message: string) =>
    response.status(401).json({
      ok: false,
      status: 401,
      message: message,
    });

  const requestHeader = "X-JWT-Token";
  const header = request.header(requestHeader);

  if (!header) {
    unauthorized(`Required ${requestHeader} header not found.`);
    return;
  }

  const decodedSession: DecodeResult = decodeSession(
    process.env.JWT_STRING!,
    header
  );

  if (
    decodedSession.type === "integrity-error" ||
    decodedSession.type === "invalid-token"
  ) {
    unauthorized(
      `Failed to decode or validate "X-JWT-Token" token. Reason: ${decodedSession.type}.`
    );
    return;
  }

  const expiration: ExpirationStatus = checkExpirationStatus(
    decodedSession.session
  );

  if (expiration === "expired") {
    unauthorized(
      `"X-JWT-Token" token has expired. Please create a new "X-JWT-Token" token.`
    );
    return;
  }

  let session: Session;

  session = decodedSession.session;

  response.locals = {
    ...response.locals,
    session: session,
  };

  next();
}
