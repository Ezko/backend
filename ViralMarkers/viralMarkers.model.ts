//importamos sequelize y la conexion a la base de datos
import { DataTypes, Model } from "sequelize";
import db from "../db/connection";

class ViralMarker extends Model {
    public IdMarcadorViral!: number;
    public NombreMarcadorViral!: string;
}

//@ts-ignore
ViralMarker.init(
    {
        IdMarcadorViral: {
            type: DataTypes.NUMBER,
            primaryKey: true,
        },
        NombreMarcadorViral: {
            type: DataTypes.STRING,
        },
    },
    {
        //@ts-ignore
        tableName: "MarcadorViral",
        sequelize: db,
    },
)

//exportamos como default
export default ViralMarker;