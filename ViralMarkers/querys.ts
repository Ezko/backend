import ViralMarker from "./viralMarkers.model"


//@ts-ignore
export const getAll = async () => await ViralMarker.findAll();

//@ts-ignore
export const findOne = async (body: any) => 
    //@ts-ignore
    await ViralMarker.findOne({
        where: {
            NombreMarcadorViral:body.NombreMarcadorViral
        },
    });

//@ts-ignore
export const getById = async (id: number) =>
    //@ts-ignore
    await ViralMarker.findByPk(id);