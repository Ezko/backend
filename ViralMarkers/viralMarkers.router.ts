import { Router } from "express";
import { requireJwtMiddleware } from "../middleware/auth.middleware";
import { getMarkers, getMarker, postMarker, putMarker, deleteMarker } from "./viralMarkers.service"

/**
 * Router Definition
 */

const viralMarkerRouter = Router();

viralMarkerRouter.use(requireJwtMiddleware);


viralMarkerRouter.get("/", getMarkers);
viralMarkerRouter.get("/:id", getMarker);
viralMarkerRouter.post("/", postMarker);
viralMarkerRouter.put("/:id", putMarker);
viralMarkerRouter.delete("/:id", deleteMarker);

export default viralMarkerRouter;