/**
 * Data Model Interfaces
 */
 import { Request, Response } from "express";
 import ViralMarker from "./viralMarkers.model";
 import * as query from "./querys";
 
 /**
  * Service Methods
  */
 
 export const getMarkers = async (req: Request, res: Response) => {
   //@ts-ignore
   const markers = await query.getAll();
 
   res.json({ markers });
 };
 
 export const getMarker = async (req: Request, res: Response) => {
    const { id } = req.params;
 
   //@ts-ignore
   const marker = await query.getById(id);
 
   if (marker) {
     res.json(marker);
   } else {
     res.status(200).json({
       msg: `No existe un turno con el id ${id}`,
     });
   }
 };
 
 export const postMarker = async (req: Request, res: Response) => {
   const { body } = req;
   const { id } = req.params;
 
   try {
     //@ts-ignore
     const existsMarker = await query.getById(id);
 
     if (existsMarker) {
       return res.status(400).json({
         msg: "Ya existe un marcador viral con el id " + id,
       });
     }
 
     //@ts-ignore
     const marker = ViralMarker.build({
        IdMarcadorViral: body.IdMarcadorViral,
        NombreMarcadorViral: body.NombreMarcadorViral,
     });
     await marker.save();
 
     res.json(marker);
   } catch (error) {
     console.log(error);
     res.status(500).json({
       msg: "Hable con el administrador",
     });
   }
 };
 
 export const putMarker = async (req: Request, res: Response) => {
   const { id } = req.params;
   const { body } = req;
 
   try {
     //@ts-ignore
     var marker = await query.getById(id);
     if (!marker) {
       //@ts-ignore
       marker = ViralMarker.build({
            IdMarcadorViral: body.IdMarcadorViral,
            NombreMarcadorViral: body.NombreMarcadorViral,
       });
       await marker.save();
     } else {
       await marker.update(body);
     }
 
     res.json(marker);
   } catch (error) {
     console.log(error);
     res.status(500).json({
       msg: "Hable con el administrador",
     });
   }
 };
 
 export const deleteMarker = async (req: Request, res: Response) => {
    const { id } = req.params;
   //@ts-ignoreclear
   const marker = await query.getById(id);
   if (!marker) {
     return res.status(200).json({
       msg: "No existe un marcador viral con el id " + id,
     });
   }
 
   await marker.update({ estado: false });
 
   await marker.destroy();
 
   res.json(marker);
 };
 